#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "usb_device.h"
#include "stm32f401_discovery.h"
#include "stm32f401_discovery_accelerometer.h"
#include "stm32f401_discovery_gyroscope.h"

// Private variables
volatile uint32_t time_var1, time_var2;
void task_1(void *p)
{
    static uint8_t i = 0;
    char c;
    while(1) {
        GPIO_SetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1);
        GPIO_ResetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1);
    }
}

void init(void) {
    SystemInit();
	GPIO_InitTypeDef  GPIO_InitStructure;
	// GPIOD Periph clock enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	// Configure PD12, PD13, PD14 and PD15 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
//    usb_device_initialize();
}


int main(void) {
	init();
    xTaskCreate(task_1, (char *)"task_1", 1000,0 ,tskIDLE_PRIORITY+1, 0);
    vTaskStartScheduler();
	return -1;
}


