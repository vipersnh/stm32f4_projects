#!python
import sys
sys.path.insert(0, '../common')

import make
import build_common

profile_main = make.profile_t()

include_dirs = ["./", "./usb_device"] + build_common.profile.include_dirs
source_dirs = build_common.profile.source_dirs
source_files = build_common.profile.source_files + ["./main.c", "./system_stm32f4xx.c",
                    "syscalls.c", "./usb_device/usb_device.c", "./stm32f4xx_it.c"]
cflags = build_common.profile.cflags
cxxflags = build_common.profile.cxxflags
ldflags =  ["--static"] + build_common.profile.ldflags
linker_scripts = build_common.profile.linker_scripts
libraries = ["-lm", "-lc"]
arflags = list()

profile_main.name = "main"
profile_main.cc = "arm-none-eabi-gcc"
profile_main.cxx = "arm-none-eabi-g++"
profile_main.ld  = "arm-none-eabi-g++"
profile_main.objcopy = "arm-none-eabi-objcopy"
profile_main.objdump = "arm-none-eabi-objdump"
profile_main.include_dirs = include_dirs
profile_main.source_dirs = source_dirs
profile_main.source_files = source_files
profile_main.cflags = cflags
profile_main.cxxflags = cxxflags
profile_main.ldflags = ldflags
profile_main.linker_scripts = linker_scripts
profile_main.libraries = libraries
profile_main.arflags = arflags
profile_main.executable = "main.elf"
profile_main.library = None
profile_main.verbose_level = make.verbosity.MINIMAL
profile_main.build_dir = "./build"
profile_main.executable_subtargets["iHex"] = [profile_main.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.hex"]
profile_main.executable_subtargets["binary"] = [profile_main.objcopy, "-O", "binary", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.bin"]
profile_main.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_main) + "main.bin",
    "0x8000000"]


profile_test = make.profile_t()

profile_test.name = "test"
profile_test.include_dirs = include_dirs
profile_test.source_dirs = source_dirs
profile_test.source_files = source_files
profile_test.cflags = cflags
profile_test.cxxflags = cxxflags
profile_test.ldflags = ldflags
profile_test.arflags = arflags
profile_test.executable = "test.elf"
profile_test.library = None
profile_test.verbose_level = make.verbosity.MINIMAL
profile_test.build_dir = "./build"

profiles = [profile_main, profile_test]

make.build(profiles_list = profiles, args = sys.argv[1:])
