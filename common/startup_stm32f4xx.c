#include <stddef.h>

void Reset_Handler( void );

void WWDG_IRQHandler(void);
void PVD_IRQHandler(void);
void TAMP_STAMP_IRQHandler(void);
void RTC_WKUP_IRQHandler(void);
void FLASH_IRQHandler(void);

void Default_Handler( void ) {
    while (1);
}

void NMI_Handler (void) __attribute__ ((weak, alias("Default_Handler")));
void HardFault_Handler (void) __attribute__((weak, alias("Default_Handler")));
void MemManage_Handler (void) __attribute__((weak, alias("Default_Handler")));
void BusFault_Handler (void) __attribute__((weak, alias("Default_Handler")));
void UsageFault_Handler (void) __attribute__((weak, alias("Default_Handler")));
void SVC_Handler (void) __attribute__((weak, alias("Default_Handler")));
void DebugMon_Handler (void) __attribute__((weak, alias("Default_Handler")));
void PendSV_Handler (void) __attribute__((weak, alias("Default_Handler")));
void SysTick_Handler (void) __attribute__((weak, alias("Default_Handler")));



extern int main( void );

#ifndef STACK_SIZE
#define STACK_SIZE                              (( size_t )0x400 )
#endif

__attribute__ ((section(".stack"))) unsigned long pulStack[STACK_SIZE];

//***************************************************************************
// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory.  The initializers for the
// ".data" segment resides immediately following the ".text" segment.
//***************************************************************************
extern unsigned int _etext;
extern unsigned int _sdata;
extern unsigned int _edata;
extern unsigned int _sbss;
extern unsigned int _ebss;

//***************************************************************************
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied main() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//*****************************************************************************
void Reset_Handler( void ) {
    unsigned int *pulSrc, *pulDest;

    //
    // Copy the data segment initializers from flash to SRAM.
    //
    pulSrc = &_etext;
    for(pulDest = &_sdata; pulDest < &_edata; )
    {
        *pulDest++ = *pulSrc++;
    }

    //
    // Zero fill the bss segment.
    //
    for(pulDest = &_sbss; pulDest < &_ebss; )
    {
        *pulDest++ = 0;
    }

    // Initialize the system
    extern void SystemInit(void);
    SystemInit();
    //
    // Call the application's entry point.
    //
    main();
    while (1);
}


//***************************************************************************
// The minimal vector table for STM32F103x Cortex-M3.
//***************************************************************************
__attribute__ ((section(".isr_vector"))) void (* const stmVectors[])(void) = {
    ( void (*)( void ))(( unsigned int )pulStack + sizeof( pulStack )),	// ()				The pop address of the STACK
    Reset_Handler,              			// Reset_Handler
    NMI_Handler,                			// NMI Handler
    HardFault_Handler,          				// Hard Fault Handler
    MemManage_Handler,          	// MPU Fault Handler
    BusFault_Handler,          	// Bus Fault Handler
    UsageFault_Handler,         		// Usage Fault Handler
    0,                         		// Reserved
    0,                          	// Reserved
    0,                          	// Reserved
    0,                          	// Reserved
    SVC_Handler,             	// SVCall Handler
    DebugMon_Handler,           	// Debug Monitor Handler
    0,                          	// Reserved
    PendSV_Handler,             	// PendSV Handler
    SysTick_Handler,            	// SysTick Handler

    Default_Handler,             	// Window WatchDog
    Default_Handler,               	// PVD through EXTI Line detection
    Default_Handler,               	// Tamper and TimeStamps through the EXTI line
    Default_Handler,               	// RTC Wakeup through the EXTI line
    Default_Handler,               	// FLASH
    Default_Handler,               	// RCC
    Default_Handler,               	// EXTI Line0
    Default_Handler,               	// EXTI Line1
    Default_Handler,               	// EXTI Line2
    Default_Handler,               	// EXTI Line3
    Default_Handler,               	// EXTI Line4
    Default_Handler,             	// DMA1 Stream 0
    Default_Handler,             	// DMA1 Stream 1
    Default_Handler,             	// DMA1 Stream 2
    Default_Handler,             	// DMA1 Stream 3
    Default_Handler,             	// DMA1 Stream 4
    Default_Handler,             	// DMA1 Stream 5
    Default_Handler,             	// DMA1 Stream 6
    Default_Handler,             	// ADC1, ADC2 and ADC3s
    Default_Handler,               	// CAN1 TX
    Default_Handler,               	// CAN1 RX0
    Default_Handler,               	// CAN1 RX1
    Default_Handler,               	// CAN1 SCE
    Default_Handler,               	// External Line[9:5]s
    Default_Handler,            	// TIM1 Break and TIM9
    Default_Handler,            	// TIM1 Update and TIM10
    Default_Handler,   			// TIM1 Trigger and Commutation and TIM11
    Default_Handler,       	// TIM1 Capture Compare
    Default_Handler,          	// TIM2
    Default_Handler,          	// TIM3
    Default_Handler,          	// TIM4
    Default_Handler,          	// I2C1 Event
    Default_Handler,          	// I2C1 Error
    Default_Handler,          	// I2C2 Event
    Default_Handler,          	// I2C2 Error
    Default_Handler,          	// SPI1
    Default_Handler,          	// SPI2
    Default_Handler,          	// USART1
    Default_Handler,          	// USART2
    Default_Handler,          	// USART3
    Default_Handler,          	// External Line[15:10]s
    Default_Handler,          	// RTC Alarm (A and B) through EXTI Line
    Default_Handler,         	// USB OTG FS Wakeup through EXTI line
    Default_Handler,      		// TIM8 Break and TIM12
    Default_Handler,       	// TIM8 Update and TIM13
    Default_Handler,   			// TIM8 Trigger and Commutation and TIM14
    Default_Handler,          	// TIM8 Capture Compare
    Default_Handler,        	// DMA1 Stream7
    Default_Handler,          	// FSMC
    Default_Handler,          	// SDIO
    Default_Handler,          	// TIM5
    Default_Handler,          	// SPI3
    Default_Handler,          	// UART4
    Default_Handler,          	// UART5
    Default_Handler,          	// TIM6 and DAC1&2 underrun errors
    Default_Handler,          	// TIM7
    Default_Handler,        	// DMA2 Stream 0
    Default_Handler,        	// DMA2 Stream 1
    Default_Handler,        	// DMA2 Stream 2
    Default_Handler,        	// DMA2 Stream 3
    Default_Handler,        	// DMA2 Stream 4
    Default_Handler,          	// Ethernet
    Default_Handler,          	// Ethernet Wakeup through EXTI line
    Default_Handler,          	// CAN2 TX
    Default_Handler,          	// CAN2 RX0
    Default_Handler,          	// CAN2 RX1
    Default_Handler,          	// CAN2 SCE
    Default_Handler,          	// USB OTG FS
    Default_Handler,        	// DMA2 Stream 5
    Default_Handler,        	// DMA2 Stream 6
    Default_Handler,        	// DMA2 Stream 7
    Default_Handler,          	// USART6
    Default_Handler,          	// I2C3 event
    Default_Handler,          	// I2C3 error
    Default_Handler,      		// USB OTG HS End Point 1 Out
    Default_Handler,       	// USB OTG HS End Point 1 In
    Default_Handler,         	// USB OTG HS Wakeup through EXTI
    Default_Handler,          	// USB OTG HS
    Default_Handler,          	// DCMI
    Default_Handler,          	// CRYP crypto
    Default_Handler,          	// Hash and Rng
    Default_Handler,
};

