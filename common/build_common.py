import make
import os
import sys

try:
    lib_path = os.environ["STM32F4_LIBS_PATH"]
except:
    print("Please define $STM32F4_LIBS_PATH environ variable")
    sys.exit(0)

try:
    cpp_lib_path = os.environ["CPP_LIBS_PATH"]
except:
    print("Please define $CPP_LIBS_PATH environ variable")
    sys.exit(0)


pwd = os.path.dirname(os.path.realpath(__file__))

stm32_drivers_path = (lib_path)
cmsis_path         = (lib_path + "./CMSIS")
stm32_lib_path     = (lib_path)
stm32_std_lib_path = (lib_path + "./STM32F4xx_StdPeriph_Driver")
stm32_usb_device_path = (lib_path + "./USB_Device")
stm32_usb_otg_path = (lib_path + "./USB_OTG")
freertos_path = (lib_path + "./FreeRTOS/FreeRTOS/Source")
freertos_path_stm32 = (lib_path + "./FreeRTOS/FreeRTOS/Demo/STM32F401xC")
freertos_path_stm32_port = (lib_path + "./FreeRTOS/FreeRTOS/Source/portable/GCC/ARM_CM4F")
freertos_path_memmang = (lib_path + "./FreeRTOS/FreeRTOS/Source/portable/MemMang")

profile = make.profile_t()
common_flags = ["-g", "-Wextra", "-Wundef", "-fno-common", "-Wno-missing-field-initializers",
                    "-mcpu=cortex-m4", "-mthumb", "-mlong-calls", "-mfloat-abi=hard", "-mfpu=fpv4-sp-d16",
                    "-mlittle-endian", "-ffunction-sections", "-fdata-sections", "-MD" ,
                    "-DSTM32F4", "-DSTM32", "-DGCC_ARMCM4", "-DUSE_HAL_DRIVER", "-DSTM32F401xC", "-DTARGET=TARGET_STM32"]

profile.cflags = common_flags
profile.cxxflags = common_flags + ["-std=c++98"]
profile.include_dirs = [(cpp_lib_path + "/cbuf"),
                        (cpp_lib_path + "/df"),
                        (cmsis_path + "/Include"),
                        (stm32_lib_path + "/Conf"),
                        (stm32_lib_path + "/Device/STM32F4xx/Include"),
                        (stm32_std_lib_path + "/inc"),
                        (stm32_drivers_path + "/BSP/Components/lsm303dlhc"),
                        (stm32_drivers_path + "/BSP/Components/l3gd20"),
                        (stm32_drivers_path + "/BSP/STM32F401-Discovery"),
                        (stm32_usb_device_path + "/Core/inc"),
                        (stm32_usb_device_path + "/Class/cdc/inc"),
                        (stm32_usb_otg_path + "/inc"),
                        (freertos_path + "/include"),
                        (freertos_path_stm32),
                        (freertos_path_stm32_port)]
profile.source_dirs = [ (stm32_std_lib_path + "/src"),
                        (stm32_drivers_path + "/BSP/Components/lsm303dlhc"),
                        (stm32_drivers_path + "/BSP/Components/l3gd20"),
                        (freertos_path),
                        (freertos_path_stm32_port),]
profile.source_files = [ 
                         (cpp_lib_path + "/cbuf/cbuf_base.cpp"),
                         (cpp_lib_path + "/cbuf/cbuf_char.cpp"),
                         (cpp_lib_path + "/cbuf/cbuf_packet.cpp"),
                         (cpp_lib_path + "/df/df_base.cpp"),
                         (pwd + "/usb_bsp.c"),
                         (stm32_drivers_path + "/BSP/STM32F401-Discovery/stm32f401_discovery.c"),
                         (stm32_drivers_path + "/BSP/STM32F401-Discovery/stm32f401_discovery_accelerometer.c"),
                         (stm32_drivers_path + "/BSP/STM32F401-Discovery/stm32f401_discovery_gyroscope.c"),
                         (freertos_path_memmang + "/heap_4.c"),
                         (pwd + "/startup_stm32f4xx.s")]
profile.ldflags     = [ ("-L" + stm32_lib_path + "/libopencm3/lib"),
                        ] + profile.cflags

profile.linker_scripts = [(pwd + "/stm32_flash.ld")]
