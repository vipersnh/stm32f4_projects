#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "main.h"
    #include "FreeRTOS.h"
    #include "task.h"
}

#include "cbuf_char.h"
#include "usb_device.h"
#include "imu.h"





// Private variables
volatile uint32_t time_var1, time_var2;
void idle_task(void *p)
{
    while(1) {
        GPIO_SetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1000);
        GPIO_ResetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1000);
    }
}

void sensor_reader_task(void *p)
{
    imu_initialize();
    imu_output_t imu_op;
    while(1) {
        vTaskDelay(2);
        get_imu_output(&imu_op);
        tx_fifo.cbuf_put_all((char*)&imu_op, sizeof(imu_op));
        usb_tx_flush();
    }
}

void init(void) {
    SystemInit();
	GPIO_InitTypeDef  GPIO_InitStructure;
	// GPIOD Periph clock enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	// Configure PD12, PD13, PD14 and PD15 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
    usb_device_initialize();
}


int main() {
	init();
    xTaskCreate(idle_task, (char *)"idle_task", 1000,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(sensor_reader_task, (char *)"sensor_reader_task", 1000,0 ,tskIDLE_PRIORITY+2, 0);
    vTaskStartScheduler();
	return -1;
}
