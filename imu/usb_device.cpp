extern "C" {
    #include <stdint.h>
    #include "usbd_core.h"
    #include "usbd_ioreq.h"
    #include "usbd_desc.h"
    #include "usbd_req.h"
    #include "usbd_conf.h"
    #include "usb_regs.h"
}
#include "cbuf_char.h"
#include "usb_device.h"
#include "stm32f4xx_usb.hpp"

uint8_t USBD_RX_BUFFER[USBD_BUFFER_SIZE];


__ALIGN_BEGIN uint8_t USBD_DeviceDesc[USB_SIZ_DEVICE_DESC] = __ALIGN_END
  {
    0x12,                       /*bLength */
    USB_DEVICE_DESCRIPTOR_TYPE, /*bDescriptorType*/
    0x00,                       /*bcdUSB */
    0x02,
    0x00,                       /*bDeviceClass*/
    0x00,                       /*bDeviceSubClass*/
    0x00,                       /*bDeviceProtocol*/
    USB_OTG_MAX_EP0_SIZE,      /*bMaxPacketSize*/
    LOBYTE(USBD_VID),           /*idVendor*/
    HIBYTE(USBD_VID),           /*idVendor*/
    LOBYTE(USBD_PID),           /*idVendor*/
    HIBYTE(USBD_PID),           /*idVendor*/
    0x00,                       /*bcdDevice rel. 2.00*/
    0x02,
    USBD_IDX_MFC_STR,           /*Index of manufacturer  string*/
    USBD_IDX_PRODUCT_STR,       /*Index of product string*/
    USBD_IDX_SERIAL_STR,        /*Index of serial number string*/
    1,                          /*bNumConfigurations*/
  } ; /* USB_DeviceDescriptor */


__ALIGN_BEGIN uint8_t USBD_LangIDDesc[USB_SIZ_STRING_LANGID] = __ALIGN_END
{
     USB_SIZ_STRING_LANGID,         
     USB_DESC_TYPE_STRING,       
     LOBYTE(USBD_LANGID_STRING),
     HIBYTE(USBD_LANGID_STRING), 
};


uint8_t *  USBD_USR_DeviceDescriptor( uint8_t speed , uint16_t *length)
{
  *length = sizeof(USBD_DeviceDesc);
  return USBD_DeviceDesc;
}


uint8_t *  USBD_USR_LangIDStrDescriptor( uint8_t speed , uint16_t *length)
{
  *length =  sizeof(USBD_LangIDDesc);  
  return USBD_LangIDDesc;
}

uint8_t *  USBD_USR_ManufacturerStrDescriptor( uint8_t speed , uint16_t *length)
{
  USBD_GetString ((uint8_t*)USBD_MANUFACTURER_STRING, USBD_StrDesc, length);
  return USBD_StrDesc;
}

uint8_t *  USBD_USR_ProductStrDescriptor( uint8_t speed , uint16_t *length)
{
 
  
  if(speed == 0)
  {   
    USBD_GetString ((uint8_t*)USBD_PRODUCT_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString ((uint8_t*)USBD_PRODUCT_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;
}

uint8_t *  USBD_USR_SerialStrDescriptor( uint8_t speed , uint16_t *length)
{
  if(speed  == USB_OTG_SPEED_HIGH)
  {    
    USBD_GetString ((uint8_t*)USBD_SERIALNUMBER_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString ((uint8_t*)USBD_SERIALNUMBER_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;
}

uint8_t *  USBD_USR_ConfigStrDescriptor( uint8_t speed , uint16_t *length)
{
  if(speed  == USB_OTG_SPEED_HIGH)
  {  
    USBD_GetString ((uint8_t*)USBD_CONFIGURATION_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString ((uint8_t*)USBD_CONFIGURATION_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;  
}

uint8_t *  USBD_USR_InterfaceStrDescriptor( uint8_t speed , uint16_t *length)
{
  if(speed == 0)
  {
    USBD_GetString ((uint8_t*)USBD_INTERFACE_HS_STRING, USBD_StrDesc, length);
  }
  else
  {
    USBD_GetString ((uint8_t*)USBD_INTERFACE_FS_STRING, USBD_StrDesc, length);
  }
  return USBD_StrDesc;  
}


__ALIGN_BEGIN uint8_t usbd_cdc_CfgDesc[39] = __ALIGN_END
{
  /*Configuration Descriptor*/
  0x09,   /* bLength: Configuration Descriptor size */
  USB_CONFIGURATION_DESCRIPTOR_TYPE,      /* bDescriptorType: Configuration */
  (32),                /* wTotalLength:no of returned bytes */
  0x00,
  0x01,   /* bNumInterfaces: 1 interface */
  0x01,   /* bConfigurationValue: Configuration value */
  0x00,   /* iConfiguration: Index of string descriptor describing the configuration */
  0x80,   /* bmAttributes: self powered */
  0x32,   /* MaxPower 0 mA */
  
  /*---------------------------------------------------------------------------*/
  
  /*Interface Descriptor */
  0x09,   /* bLength: Interface Descriptor size */
  USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: Interface */
  0x00,   /* bInterfaceNumber: Number of Interface */
  0x00,   /* bAlternateSetting: Alternate setting */
  0x02,   /* bNumEndpoints: Two endpoints used, excluding zero */
  0xFF,   /* bInterfaceClass: Vendor specfic Class */
  0xFF,   /* bInterfaceSubClass: Abstract Control Model */
  0xFF,   /* bInterfaceProtocol: Common AT commands */
  0x04,   /* iInterface: */
  
  /*Endpoint 1 Descriptor*/
  0x07,                           /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
  0x81,                           /* bEndpointAddress */
  0x02,                           /* bmAttributes: Bulk */
  LOBYTE(64),     /* wMaxPacketSize: */
  HIBYTE(64),
  0x0,                            /* bInterval : some value */
  
   /*Endpoint 2 Descriptor*/
  0x07,                              /* bLength: Endpoint Descriptor size */
  USB_ENDPOINT_DESCRIPTOR_TYPE,      /* bDescriptorType: Endpoint */
  0x01,                              /* bEndpointAddress */
  0x02,                              /* bmAttributes: Bulk */
  LOBYTE(64),  /* wMaxPacketSize: */
  HIBYTE(64),
  0x0,                               /* bInterval: ignore for Bulk transfer */

  } ;


static uint8_t  *USBD_cdc_GetCfgDesc (uint8_t speed, uint16_t *length)
{
  *length = sizeof (usbd_cdc_CfgDesc);
  return usbd_cdc_CfgDesc;
}

USB_OTG_CORE_HANDLE  USB_OTG_dev;

USBD_DEVICE USR_desc =
{
  USBD_USR_DeviceDescriptor,
  USBD_USR_LangIDStrDescriptor, 
  USBD_USR_ManufacturerStrDescriptor,
  USBD_USR_ProductStrDescriptor,
  USBD_USR_SerialStrDescriptor,
  USBD_USR_ConfigStrDescriptor,
  USBD_USR_InterfaceStrDescriptor,
  
};

static uint8_t  usb_device_class_init (void  *pdev, 
                               uint8_t cfgidx)
{
  uint8_t *pbuf;

  /* Open EP IN */
  DCD_EP_Open((USB_OTG_CORE_HANDLE *)pdev, 0x81, 64, USB_OTG_EP_BULK);
  
  /* Open EP OUT */
  DCD_EP_Open((USB_OTG_CORE_HANDLE *)pdev, 0x01, 64, USB_OTG_EP_BULK);

  /* Prepare Out endpoint to receive next packet */
  DCD_EP_PrepareRx((USB_OTG_CORE_HANDLE *)pdev, 0x01, (uint8_t*)(USBD_RX_BUFFER), 64);

  return USBD_OK;
}

static uint8_t  usb_device_class_deinit (void  *pdev, 
                                 uint8_t cfgidx)
{
  DCD_EP_Close((USB_OTG_CORE_HANDLE *)pdev, 0x81);
  DCD_EP_Close((USB_OTG_CORE_HANDLE *)pdev, 0x01);
  return USBD_OK;
}

uint32_t  usbd_cdc_AltSet;

static uint8_t  usb_device_class_setup(void  *pdev, 
                                USB_SETUP_REQ *req)
{
  uint16_t len;
  uint8_t  *pbuf;
  
  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
    /* CDC Class Requests -------------------------------*/
  case USB_REQ_TYPE_CLASS :
      return USBD_OK;
      
    /* Standard Requests -------------------------------*/
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
    {
    case USB_REQ_GET_DESCRIPTOR: 
        pbuf = usbd_cdc_CfgDesc + 9 + (9 * 1);
        len = MIN(32 , req->wLength);
      USBD_CtlSendData ((USB_OTG_CORE_HANDLE *)pdev, 
                        pbuf,
                        len);
      break;
      
    case USB_REQ_GET_INTERFACE :
      USBD_CtlSendData ((USB_OTG_CORE_HANDLE *)pdev,
                        (uint8_t *)&usbd_cdc_AltSet,
                        1);
      break;
      
    case USB_REQ_SET_INTERFACE :
      if ((uint8_t)(req->wValue) < USBD_ITF_MAX_NUM)
      {
        usbd_cdc_AltSet = (uint8_t)(req->wValue);
      }
      else
      {
        /* Call the error management function (command will be nacked */
        USBD_CtlError ((USB_OTG_CORE_HANDLE *)pdev, req);
      }
      break;
    }
  }
  return USBD_OK;
}

static uint8_t usb_device_class_ep0_tx_sent(void *pdev)
{
    return USBD_OK;
}

static uint8_t usb_device_class_ep0_rx_ready(void *pdev)
{
    return USBD_OK;
}

static uint8_t usb_device_class_datain(void *pdev, uint8_t ep_num)
{
   
    return USBD_OK;
}

static uint8_t usb_device_class_dataout(void *pdev, uint8_t ep_num)
{
    uint16_t USB_Rx_Cnt;
    
    /* Get the received data buffer and update the counter */
    USB_Rx_Cnt = ((USB_OTG_CORE_HANDLE*)pdev)->dev.out_ep[ep_num].xfer_count;
    
    rx_fifo.cbuf_put_all((char *)USBD_RX_BUFFER, USB_Rx_Cnt);

    /* Prepare Out endpoint to receive next packet */
    DCD_EP_PrepareRx((USB_OTG_CORE_HANDLE *)pdev, 0x01, (uint8_t*)(USBD_RX_BUFFER), 64);
    return USBD_OK;
}

void usb_tx_flush()
{
    uint16_t tx_length;
    uint8_t  tx_buffer[64];
    
    /* Prepare the available data buffer to be sent on IN endpoint */
    tx_length = tx_fifo.cbuf_get_all((char *)tx_buffer, 64);
    DCD_EP_Tx (&USB_OTG_dev, 0x81, tx_buffer, tx_length);
}

static uint8_t usb_device_class_sof(void *pdev)
{
    return USBD_OK;
}

static USBD_Class_cb_TypeDef  USBD_CDC_cb = 
{
  usb_device_class_init,
  usb_device_class_deinit,
  usb_device_class_setup,
  usb_device_class_ep0_tx_sent,
  usb_device_class_ep0_rx_ready,
  usb_device_class_datain,
  usb_device_class_dataout,
  usb_device_class_sof,
  NULL,
  NULL,     
  USBD_cdc_GetCfgDesc,
};

void usb_device_usr_init()
{
}

void usb_device_usr_reset(uint8_t val)
{
}

void usb_device_usr_device_configured()
{
}

void usb_device_usr_device_suspended()
{
}

void usb_device_usr_device_resumed()
{
}

void usb_device_usr_device_connected()
{
}

void usb_device_usr_device_disconnected()
{
}

static USBD_Usr_cb_TypeDef USR_cb =
{
  usb_device_usr_init,
  usb_device_usr_reset,
  usb_device_usr_device_configured,
  usb_device_usr_device_suspended,
  usb_device_usr_device_resumed,
  usb_device_usr_device_connected,
  usb_device_usr_device_disconnected,
};

cbuf_char_t tx_fifo;
cbuf_char_t rx_fifo;

void usb_device_initialize()
{

    tx_fifo.cbuf_init(1024);
    rx_fifo.cbuf_init(1024);
    stm32f4xx_usb_init();
    return;
}
