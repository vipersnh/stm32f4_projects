#ifndef __IMU_H_
#define __IMU_H_
typedef struct {
    struct {
        int16_t    alpha;
        int16_t    gamma;
        int16_t    theta;
    } angle;

    struct {
        int16_t    x;
        int16_t    y;
        int16_t    z;
    } pos;
} imu_kalman_output_t;

typedef struct {
    struct {
        int16_t    x;
        int16_t    y;
        int16_t    z;
    } accelero;
    struct {
        int16_t    x;
        int16_t    y;
        int16_t    z;
    } gyro;
    struct {
        int16_t    x;
        int16_t    y;
        int16_t    z;
    } magneto;
} imu_output_t;

bool imu_initialize(void);
bool get_imu_output(imu_output_t *op);


#endif /* __IMU_H_ */
