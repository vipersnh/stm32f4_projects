extern "C" {
    #include <stdint.h>
    }
    #include "stm32f401_discovery.h"
    #include "stm32f401_discovery_accelerometer.h"
    #include "stm32f401_discovery_gyroscope.h"
}
#include "imu.h"

bool imu_initialize(void)
{
    BSP_ACCELERO_Init();
    BSP_GYRO_Init();
}

bool get_imu_output(imu_output_t *op)
{
    int16_t acc_data[3];
    float gyro_data[3];
    BSP_ACCELERO_GetXYZ(acc_data);
    BSP_GYRO_GetXYZ(gyro_data);
    op->accelero.x = acc_data[0];
    op->accelero.y = acc_data[1];
    op->accelero.z = acc_data[2];

    op->gyro.x     = gyro_data[0];
    op->gyro.y     = gyro_data[1];
    op->gyro.z     = gyro_data[2];

    op->magneto.x  = 0;
    op->magneto.y  = 0;
    op->magneto.z  = 0;
    return true;
}
