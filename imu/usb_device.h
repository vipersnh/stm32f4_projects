#ifndef _USB_DEVICE_H_
#define _USB_DEVICE_H_

#define USBD_VID                        0x0123
#define USBD_PID                        0x0234

#define USBD_LANGID_STRING              0x409
#define USBD_MANUFACTURER_STRING        "vipersnh"

#define USBD_PRODUCT_HS_STRING          "STM32 USB Test in HS mode"
#define USBD_SERIALNUMBER_HS_STRING     "00000000050B"

#define USBD_PRODUCT_FS_STRING          "STM32 USB Test in FS Mode"
#define USBD_SERIALNUMBER_FS_STRING     "00000000050C"

#define USBD_CONFIGURATION_HS_STRING    "USB Test"
#define USBD_INTERFACE_HS_STRING        "USB Test interface"

#define USBD_CONFIGURATION_FS_STRING    "USB Test"
#define USBD_INTERFACE_FS_STRING        "USB Test interface"

#define USBD_HOST_TO_STM32_EP_PACKET_SIZE   64
#define USBD_HOST_TO_STM32_EP           0b00000001  /* OUT Endpoint */
#define USBD_STM32_TO_HOST_EP_PACKET_SIZE   64
#define USBD_STM32_TO_HOST_EP           0b10000001  /* IN Endpoint */



#define USBD_BUFFER_SIZE                1024
extern uint8_t USBD_RX_BUFFER[USBD_BUFFER_SIZE];

extern cbuf_char_t tx_fifo;
extern cbuf_char_t rx_fifo;

extern void usb_device_initialize();
extern void usb_tx_flush();

#endif
