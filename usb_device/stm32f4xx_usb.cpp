extern "C" {
    #include <stdint.h>
    #include <stdio.h>
    #include <string.h>
    #include <stddef.h>
    #include "usb_bsp.h"
}
#include "df_base.h"
#include "cbuf_packet.h"
#include "stm32f4xx_usb.hpp"
#include "usb_core_common.hpp"
#include "usb_core_device.hpp"
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_device.hpp"


void stm32f4xx_usb_activate_ctrl_ep(void);
void stm32f4xx_usb_reset_endpoint_init(void);
void stm32f4xx_usb_device_schedule_data_in(uint8_t ep_num);
void stm32f4xx_usb_device_out_start(uint8_t ep_num, uint8_t packet_size);
void stm32f4xx_usb_device_activate_dataout(uint8_t ep_num);


static usb_device_descriptor_t g_usb_device_descriptor;
static usb_device_endpoint_descriptor_t g_usb_device_endpoints[3];
static usb_device_interface_descriptor_t g_usb_interface[1];
static usb_device_configuration_descriptor_t g_usb_configuration;
static usb_device_hardware_handlers_t g_usb_device_hardware_handlers;
static usb_device_state_handlers_t g_usb_device_state_handlers;
static usb_device_core_handlers_t g_usb_device_core_handlers;
static usb_device_string_descriptor_zero_t g_usb_device_str_descriptor0;
static usb_device_string_descriptor_t g_usb_device_str_descriptors[6];
static uint8_t                        g_usb_device_num_str_descriptors = 0;

static const char usb_device_manufacturer_str[] = "Some manufacturer";
static const char usb_device_product_str[]      = "Some product";
static const char usb_device_serialnumber_str[] = "Some serial number";
static const char usb_device_interface0_str[]   = "Some interface";
static const char usb_device_configuration1_str[] = "Some configuration";


void usb_comm_callback_ep_activate(uint8_t ep_num)
{
    stm32f4xx_usb_device_activate_dataout(ep_num);
}


void usb_comm_callback_ep_dataout_start(uint8_t ep_num, uint8_t buffer_len)
{
    usb_device_start_dataout_transfer(ep_num, buffer_len);
}

void usb_comm_callback_ep_datain_start(uint8_t ep_num, uint8_t * buffer, uint8_t buffer_len)
{
    usb_device_start_datain_transfer(ep_num, buffer, buffer_len);
}


void stm32f4xx_usb_device_set_address(uint8_t addr)
{
    stm32f4xx_usb_dcfg_t dcfg = {0};
    addr &= 0x7F;
    dcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_DCFG);
    dcfg.b.dad = addr;
    U32_REG_WRITE(REG_USB_OTG_FS_DCFG, dcfg.d32);
}

void stm32f4xx_usb_device_schedule_data_in(uint8_t ep_num)
{
    ep_num &= 0x7F;
    usb_assert(ep_num<=3);
    usb_device_endpoint_descriptor_t * ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
    usb_assert(ep);
    usb_assert(ep->datain_packet_fifo.cbuf_get_num_elem());

    cbuf_packet_item_t * packet_item = ep->datain_packet_fifo.cbuf_get_first_packet();

    ep->datain_byte_stream.cbuf_put_all((char*)packet_item->packet_buf, packet_item->len);

    /* Enable TxFiFo empty interrupt here so that packets are filled from the interrupt context */
    stm32f4xx_usb_diepempmsk_t diepempmsk = {0};
    diepempmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPEMPMSK);
    diepempmsk.b.ineptxfem |= packet_item->len ? 1<<ep_num:0;
    U32_REG_WRITE(REG_USB_OTG_FS_DIEPEMPMSK, diepempmsk.d32);

    stm32f4xx_usb_diepmsk0_3_t diepmsk = {0};
    diepmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPMSK);
    diepmsk.b.txfe = packet_item->len ? 1 : 0;
    U32_REG_WRITE(REG_USB_OTG_FS_DIEPMSK, diepmsk.d32);
    
    if (ep_num==0) {
        /* Populate the transfer size for the endpoint and then enable the IN endpoint */
        stm32f4xx_usb_dieptsiz0_t dieptsiz0 = {0};
        dieptsiz0.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPTSIZ0);
        dieptsiz0.b.pktcnt = packet_item->len/ep->datain_max_packet_size + 1;
        dieptsiz0.b.xfrsiz = packet_item->len;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTSIZ0, dieptsiz0.d32);
    
        stm32f4xx_usb_diepctl0_t diepctl0 = {0};
        diepctl0.b.cnak = 1;
        diepctl0.b.epena = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPCTL0, diepctl0.d32);
    } else {
        uint32_t * ptr_diepctl  = NULL;
        uint32_t * ptr_dieptsiz = NULL;
        stm32f4xx_usb_dieptsiz1_3_t dieptsiz = {0};
        stm32f4xx_usb_diepctl1_3_t diepctl = {0};
        switch(ep_num) {
            case 1:
                ptr_diepctl  = (uint32_t*)REG_USB_OTG_FS_DIEPCTL1;
                ptr_dieptsiz = (uint32_t*)REG_USB_OTG_FS_DIEPTSIZ1;
                break;
            case 2:
                ptr_diepctl  = (uint32_t*)REG_USB_OTG_FS_DIEPCTL2;
                ptr_dieptsiz = (uint32_t*)REG_USB_OTG_FS_DIEPTSIZ2;
                break;
            case 3:
                ptr_diepctl  = (uint32_t*)REG_USB_OTG_FS_DIEPCTL3;
                ptr_dieptsiz = (uint32_t*)REG_USB_OTG_FS_DIEPTSIZ3;
                break;
            default:
                usb_assert(0);
        }
        dieptsiz.d32 = U32_REG_READ(ptr_dieptsiz);
        dieptsiz.b.pktcnt = 1;
        dieptsiz.b.xfrsiz = packet_item->len;
        U32_REG_WRITE(ptr_dieptsiz, dieptsiz.d32);

        diepctl.b.mpsiz  = ep->datain_max_packet_size;
        diepctl.b.usbaep = 1;
        diepctl.b.eptyp  = ep->datain_std.bmAttributes;
        diepctl.b.txfnum  = ep_num & 0x7F;
        diepctl.b.cnak = 1;
        diepctl.b.epena = 1;
        U32_REG_WRITE(ptr_diepctl, diepctl.d32);
    }

    ep->datain_packet_fifo.cbuf_pop_packet_buf(NULL, 0, 0);
}

void stm32f4xx_usb_device_schedule_data_out(uint8_t ep_num, uint8_t size)
{
    usb_device_endpoint_descriptor_t * ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
    usb_assert(ep);
    /* Populate the dataout-byte stream for reception of the packet via dataout byte stream */
    switch (ep_num) {
        case 0:
            {
                stm32f4xx_usb_doeptsiz0_t doeptsiz0 = {0};
                doeptsiz0.b.stupcnt = 3;
                doeptsiz0.b.xfrsiz = size;
                doeptsiz0.b.pktcnt = size/ep->dataout_max_packet_size+1;
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPTSIZ0, doeptsiz0.d32);

                stm32f4xx_usb_doepctl0_t doepctl0 = {0};
                doepctl0.b.cnak = 1;
                doepctl0.b.epena = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPCTL0, doepctl0.d32);
            }
            break;

        case 1:
            {
                stm32f4xx_usb_doeptsiz1_3_t doeptsiz = {0};
                doeptsiz.b.xfrsiz = size;
                doeptsiz.b.pktcnt = 1;
                doeptsiz.b.rxdpid_stupcnt = 0;
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPTSIZ1, doeptsiz.d32);

                stm32f4xx_usb_doepctl1_3_t doepctl = {0};
                doepctl.b.mpsiz     = ep->dataout_max_packet_size;
                doepctl.b.usbaep    = 1;
                doepctl.b.eptyp     = ep->dataout_std.bmAttributes;
                doepctl.b.sd0pid_sevnfrm = 1;
                doepctl.b.cnak      = 1;
                doepctl.b.epena     = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPCTL1, doepctl.d32);
            }
            break;
        default:
            usb_assert(0);
    }
}

void stm32f4xx_usb_device_out_start(uint8_t ep_num, uint8_t packet_size)
{

    usb_assert(ep_num==0);
    usb_device_endpoint_descriptor_t * ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
    stm32f4xx_usb_doeptsiz0_t doeptsiz0 = {0};
    doeptsiz0.b.stupcnt = 3;
    doeptsiz0.b.pktcnt = packet_size/ep->dataout_max_packet_size + 1;
    doeptsiz0.b.xfrsiz = packet_size;
    U32_REG_WRITE(REG_USB_OTG_FS_DOEPTSIZ0, doeptsiz0.d32);

    stm32f4xx_usb_doepctl0_t doepctl0 = {0};
    doepctl0.b.cnak = 1;
    doepctl0.b.epena = 1;
    U32_REG_WRITE(REG_USB_OTG_FS_DOEPCTL0, doepctl0.d32);
}

void stm32f4xx_usb_populate_string_descriptor_zero(usb_device_string_descriptor_zero_t * descriptor)
{
    descriptor->num_lang_id = 1;
    descriptor->std.bLength = 2+2;
    descriptor->std.bDescriptorType = 0x03;
    descriptor->std.wLANGID[0] = 0x409;
}

void stm32f4xx_usb_populate_string_descriptors(usb_device_string_descriptor_t * descriptors, uint8_t * num_descriptors)
{
    uint8_t str_idx;

    str_idx = 1;
    descriptors[str_idx-1].std.bDescriptorType = 0x03;
    for (uint8_t idx=0; idx<=strlen(usb_device_manufacturer_str); idx++) {
        descriptors[str_idx-1].std.bString[2*idx] = usb_device_manufacturer_str[idx];
        descriptors[str_idx-1].std.bString[2*idx+1] = 0x00;
    }
    descriptors[str_idx-1].std.bLength = 2+2*sizeof(usb_device_manufacturer_str);
    (*num_descriptors) ++;

    str_idx = 2;
    descriptors[str_idx-1].std.bDescriptorType = 0x03;
    for (uint8_t idx=0; idx<=strlen(usb_device_product_str); idx++) {
        descriptors[str_idx-1].std.bString[2*idx] = usb_device_product_str[idx];
        descriptors[str_idx-1].std.bString[2*idx+1] = 0x00;
    }
    descriptors[str_idx-1].std.bLength = 2+2*sizeof(usb_device_product_str);
    (*num_descriptors) ++;

    str_idx = 3;
    descriptors[str_idx-1].std.bDescriptorType = 0x03;
    for (uint8_t idx=0; idx<=strlen(usb_device_serialnumber_str); idx++) {
        descriptors[str_idx-1].std.bString[2*idx] = usb_device_serialnumber_str[idx];
        descriptors[str_idx-1].std.bString[2*idx+1] = 0x00;
    }
    descriptors[str_idx-1].std.bLength = 2+2*sizeof(usb_device_serialnumber_str);
    (*num_descriptors) ++;

    str_idx = 4;
    descriptors[str_idx-1].std.bDescriptorType = 0x03;
    for (uint8_t idx=0; idx<=strlen(usb_device_interface0_str); idx++) {
        descriptors[str_idx-1].std.bString[2*idx] = usb_device_interface0_str[idx];
        descriptors[str_idx-1].std.bString[2*idx+1] = 0x00;
    }
    descriptors[str_idx-1].std.bLength = 2+2*sizeof(usb_device_interface0_str);
    (*num_descriptors) ++;

    str_idx = 5;
    descriptors[str_idx-1].std.bDescriptorType = 0x03;
    for (uint8_t idx=0; idx<=strlen(usb_device_configuration1_str); idx++) {
        descriptors[str_idx-1].std.bString[2*idx] = usb_device_configuration1_str[idx];
        descriptors[str_idx-1].std.bString[2*idx+1] = 0x00;
    }
    descriptors[str_idx-1].std.bLength = 2+2*sizeof(usb_device_configuration1_str);
    (*num_descriptors) ++;
}

void stm32f4xx_usb_populate_device_descriptors(usb_device_descriptor_t * descriptor)
{
    descriptor->std.bLength             = USB_DEVICE_DESCRIPTOR_LENGTH;/* Valid length */
    descriptor->std.bDescriptorType     = 1;   /* Device descriptor */
    descriptor->std.bcdUSB              = 0x0000;       /* Spec version */
    descriptor->std.bDeviceClass        = 0x00;   /* Class information in Interface Descriptor */
    descriptor->std.bDeviceSubClass     = 0x00;/* Class information in Interface Descriptor */
    descriptor->std.bDeviceProtocol     = 0x00;
    descriptor->std.bMaxPacketSize0     = 64;  /* Max EP0 Packet Size */
    descriptor->std.idVendor            = 0xF0F0;     /* Vendor ID */
    descriptor->std.idProduct           = 0xA0B0;    /* Product ID */
    descriptor->std.bcdDevice           = 0x0200;    /* Device Release No */
    descriptor->std.iManufacturer       = 0x01;  /* Index of Manufacturer String */
    descriptor->std.iProduct            = 0x02;       /* Index of Product String */
    descriptor->std.iSerialNumber       = 0x03;     /* Index to Serial Number */
    descriptor->std.bNumConfigurations  = 1;/* Number of possible configurations */
}

void stm32f4xx_usb_populate_endpoint_descriptor(usb_device_endpoint_descriptor_t * descriptor, uint8_t addr,
    usb_device_endpoint_type_enum_t endpoint_type, uint8_t num_packets, uint16_t max_packet_size)
{
    
    descriptor->datain_std.bLength             = USB_ENDPOINT_DESCRIPTOR_LENGTH;            /* Valid length */
    descriptor->datain_std.bDescriptorType     = 5;    /* Endpoint descriptor */
    descriptor->datain_std.bEndpointAddress    = addr ? addr | 0x80 : addr; /* Endpoint address */
    descriptor->datain_std.bmAttributes        = endpoint_type;       /* Control endpoint */
    descriptor->datain_std.wMaxPacketSize      = max_packet_size;    /* Packet size */
    descriptor->datain_std.bInterval           = 10;          /* Ignored for Bulk & Control */

    descriptor->dataout_std.bLength             = USB_ENDPOINT_DESCRIPTOR_LENGTH;            /* Valid length */
    descriptor->dataout_std.bDescriptorType     = 5;    /* Endpoint descriptor */
    descriptor->dataout_std.bEndpointAddress    = addr; /* Endpoint address */
    descriptor->dataout_std.bmAttributes        = endpoint_type;       /* Control endpoint */
    descriptor->dataout_std.wMaxPacketSize      = max_packet_size;    /* Packet size */
    descriptor->dataout_std.bInterval           = 10;          /* Ignored for Bulk & Control */

    descriptor->state                          = USB_DEVICE_EP_STATE_IDLE;
    descriptor->datain_state                   = USB_DEVICE_EP_TRANSACTION_IDLE;
    descriptor->dataout_state                  = USB_DEVICE_EP_TRANSACTION_IDLE;
    descriptor->datain_max_packet_size         = max_packet_size;
    descriptor->dataout_max_packet_size        = max_packet_size;

    descriptor->datain_packet_fifo.cbuf_init(num_packets);
    descriptor->datain_packet_fifo.cbuf_setup_packet_length(max_packet_size);
    descriptor->datain_byte_stream.cbuf_init(max_packet_size);

    descriptor->dataout_packet_fifo.cbuf_init(num_packets);
    descriptor->dataout_packet_fifo.cbuf_setup_packet_length(max_packet_size);
    descriptor->dataout_byte_stream.cbuf_init(max_packet_size);
}

void stm32f4xx_usb_populate_interface_descriptor(usb_device_interface_descriptor_t * intf_descriptor,
    usb_device_endpoint_descriptor_t * endpoint_descriptors, uint8_t num_endpoints)
{
    intf_descriptor->std.bLength            = USB_INTERFACE_DESCRIPTOR_LENGTH;                   /* Valid length */
    intf_descriptor->std.bDescriptorType    = 4;           /* Interface descriptor */
    intf_descriptor->std.bInterfaceNumber   = 0;          /* Zero-based number of this interface */
    intf_descriptor->std.bAlternateSetting  = 0;         /* Valued for alternative settting for this interface */
    intf_descriptor->std.bNumEndpoints      = num_endpoints; /* Number of endpoints used by this interface */
    intf_descriptor->std.bInterfaceClass    = 0x02;        /* Vendor specific class */
    intf_descriptor->std.bInterfaceSubClass = 0x02;        /* Vendor specific class */
    intf_descriptor->std.bInterfaceProtocol = 0x02;
    intf_descriptor->std.iInterface         = 4;                /* Index of string descriptor describing this interface */
    intf_descriptor->num_endpoints          = num_endpoints;
    intf_descriptor->endpoint_descriptors   = endpoint_descriptors;
}

void stm32f4xx_usb_populate_configuration_descriptor(usb_device_configuration_descriptor_t * conf_descriptor, 
    usb_device_interface_descriptor_t * intf_descriptor, uint8_t num_interfaces)
{
    uint16_t wTotalLength = USB_CONFIGURATION_DESCRIPTOR_LENGTH;
    uint8_t  intf_idx, ep_idx;
    for (intf_idx=0; intf_idx<num_interfaces; intf_idx++) {
        wTotalLength += USB_INTERFACE_DESCRIPTOR_LENGTH;
        for (ep_idx=0; ep_idx<intf_descriptor->std.bNumEndpoints; ep_idx++) {
            wTotalLength += USB_ENDPOINT_DESCRIPTOR_LENGTH;
        }
    }
    conf_descriptor->std.bLength                = USB_CONFIGURATION_DESCRIPTOR_LENGTH;
    conf_descriptor->std.bDescriptorType        = 2;
    conf_descriptor->std.wTotalLength           = wTotalLength;
    conf_descriptor->std.bNumInterfaces         = num_interfaces;
    conf_descriptor->std.bConfigurationValue    = 1;
    conf_descriptor->std.iConfiguration         = 5;
    conf_descriptor->std.bmAttributes           = 0xA0;
    conf_descriptor->std.bMaxPower              = 50;
    conf_descriptor->num_interfaces             = num_interfaces;
    conf_descriptor->interface_descriptors      = intf_descriptor;
}

void stm32f4xx_usb_device_activate_dataout(uint8_t ep_num)
{
    stm32f4xx_usb_daintmsk_t daintmsk = {0};
    daintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DAINTMSK);
    ep_num &= 0x7F;
    daintmsk.b.iepint |= 1<<ep_num;
    daintmsk.b.oepint |= 1<<ep_num;
    U32_REG_WRITE(REG_USB_OTG_FS_DAINTMSK, daintmsk.d32);
}

void stm32f4xx_usb_isr(void)
{
    stm32f4xx_usb_gintsts_t gintr_status, clear_sts;
    gintr_status.d32 = U32_REG_READ(REG_USB_OTG_FS_GINTSTS) & U32_REG_READ(REG_USB_OTG_FS_GINTMSK);
    if (!gintr_status.d32) {
        DF_DEBUG_NEXT_EVENT(USBD_SPURIOUS_INTR);
        return;
    } else {
        if (gintr_status.b.oepint) {
            DF_DEBUG_NEXT_EVENT(USBD_OUTEP_INTR);
            stm32f4xx_usb_daint_t daint = {0};
            stm32f4xx_usb_daintmsk_t daintmsk = {0};
            daint.d32 = U32_REG_READ(REG_USB_OTG_FS_DAINT);
            daintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DAINTMSK);

            daint.d32 &= daintmsk.d32;
            
            uint8_t oepint = daint.b.oepint;
            uint8_t ep_num = 0;
            uint32_t * ptr_doepint = NULL;
            
            stm32f4xx_usb_doepint0_3_t doepint = {0};
            stm32f4xx_usb_doepmsk0_3_t doepmsk = {0};

            
            doepmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DOEPMSK);
            while (oepint) {
                switch(ep_num) {
                    case 0:
                        ptr_doepint = (uint32_t*)REG_USB_OTG_FS_DOEPINT0;
                        break;
                    case 1:
                        ptr_doepint = (uint32_t*)REG_USB_OTG_FS_DOEPINT1;
                        break;
                    case 2:
                        ptr_doepint = (uint32_t*)REG_USB_OTG_FS_DOEPINT2;
                        break;
                    case 3:
                        ptr_doepint = (uint32_t*)REG_USB_OTG_FS_DOEPINT3;
                        break;
                    default:
                        usb_assert(0);
                        break;
                }
                doepint.d32 = U32_REG_READ(ptr_doepint);
                doepint.d32 &= doepmsk.d32;
                if (oepint & 0x01) {

                    if (doepint.b.xfrc || doepint.b.stup) {
                        /* Packet reception complete, store it into packet fifo of corresponding ep */
                        usb_device_endpoint_descriptor_t * ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
                        ep->dataout_packet_fifo.cbuf_push_packet_buf(NULL, 0);
                        cbuf_packet_item_t * packet_item = ep->dataout_packet_fifo.cbuf_get_last_packet();
                        packet_item->len = ep->dataout_byte_stream.cbuf_get_all((char*)packet_item->packet_buf, 
                            ep->dataout_packet_fifo.cbuf_get_packet_buf_length());
                        usb_assert(ep->dataout_byte_stream.cbuf_get_num_elem()==0);
                        
                        if (doepint.b.xfrc) {
                            DF_DEBUG_NEXT_EVENT(USBD_ORIG_DATAOUTSTAGE_CALL);
        
                            /* Clear the interrupt bit */
                            doepint.b.xfrc = 0;
                            stm32f4xx_usb_doepint0_3_t clear_sts = {0};
                            clear_sts.b.xfrc = 1;
                            U32_REG_WRITE(ptr_doepint, clear_sts.d32);
                            cbuf_packet_item_t * packet_item = ep->dataout_packet_fifo.cbuf_get_last_packet();
                            g_usb_device_handle->usb_device_handle_ep_dataout_complete(ep_num, packet_item->packet_buf, packet_item->len);
                            ep->dataout_packet_fifo.cbuf_pop_packet_buf(NULL, 0, 0);
                        }
                        
                        if (doepint.b.stup) {
                            DF_DEBUG_NEXT_EVENT(USBD_ORIG_SETUPSTAGE_CALL);
        
                            /* Clear the interrupt bit */
                            doepint.b.stup = 0;
                            stm32f4xx_usb_doepint0_3_t clear_sts = {0};
                            clear_sts.b.stup = 1;
                            U32_REG_WRITE(ptr_doepint, clear_sts.d32);
                            
                            cbuf_packet_item_t * packet_item = ep->dataout_packet_fifo.cbuf_get_last_packet();
                            g_usb_device_handle->usb_device_handle_setup_request(
                                (usb_setup_packet_t*)packet_item->packet_buf, 0);
                            ep->dataout_packet_fifo.cbuf_pop_packet_buf(NULL, 0, 0);
                        }
                    }
    
                    if (doepint.b.otepdis) {
                        /* Ignore for now */
                        /* Clear the interrupt bit */
                        doepint.b.otepdis = 0;
                        stm32f4xx_usb_doepint0_3_t clear_sts = {0};
                        clear_sts.b.otepdis = 1;
                        U32_REG_WRITE(ptr_doepint, clear_sts.d32);
                    }
    
                    if (doepint.d32) {
                        usb_assert(0);
                    }
                }
                oepint >>= 1;
                ep_num++;
            }

            /* Clear the interrupt bit */
            gintr_status.b.oepint = 0;
        }

        if (gintr_status.b.iepint) {
            DF_DEBUG_NEXT_EVENT(USBD_INEP_INTR);
            stm32f4xx_usb_daint_t daint = {0};
            stm32f4xx_usb_daintmsk_t daintmsk = {0};
            daint.d32 = U32_REG_READ(REG_USB_OTG_FS_DAINT);
            daintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DAINTMSK);

            daint.d32 &= daintmsk.d32;
            
            uint8_t iepint = daint.b.iepint;
            uint8_t ep_num = 0;
            uint32_t * ptr_diepint = NULL, *ptr_dtxfsts = NULL, *ptr_dfifo = NULL;

            stm32f4xx_usb_diepint0_3_t diepint0 = {0};
            stm32f4xx_usb_diepmsk0_3_t diepmsk = {0};

            diepmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPMSK);
            while (iepint) {
                if (iepint&0x01) {
                    switch (ep_num) {
                        case 0:
                            ptr_diepint = (uint32_t*)REG_USB_OTG_FS_DIEPINT0;
                            ptr_dtxfsts = (uint32_t*)REG_USB_OTG_FS_DTXFSTS0;
                            ptr_dfifo   = (uint32_t*)REG_USB_OTG_FS_DFIFO0;
                            break;
                        case 1:
                            ptr_diepint = (uint32_t*)REG_USB_OTG_FS_DIEPINT1;
                            ptr_dtxfsts = (uint32_t*)REG_USB_OTG_FS_DTXFSTS1;
                            ptr_dfifo   = (uint32_t*)REG_USB_OTG_FS_DFIFO1;
                            break;
                        case 2:
                            ptr_diepint = (uint32_t*)REG_USB_OTG_FS_DIEPINT2;
                            ptr_dtxfsts = (uint32_t*)REG_USB_OTG_FS_DTXFSTS2;
                            ptr_dfifo   = (uint32_t*)REG_USB_OTG_FS_DFIFO2;
                            break;
                        case 3:
                            ptr_diepint = (uint32_t*)REG_USB_OTG_FS_DIEPINT3;
                            ptr_dtxfsts = (uint32_t*)REG_USB_OTG_FS_DTXFSTS3;
                            ptr_dfifo   = (uint32_t*)REG_USB_OTG_FS_DFIFO3;
                            break;
                        default:
                            usb_assert(0);
                    }
                    diepint0.d32 = U32_REG_READ(ptr_diepint);
                    diepint0.d32 &= diepmsk.d32;

                    if (diepint0.b.ittxfe) {
                        /* Clear the interrupt bit */
                        usb_assert(0);
                        diepint0.b.ittxfe = 0;
                        stm32f4xx_usb_diepmsk0_3_t clear_sts = {0};
                        clear_sts.b.ittxfe = 1;
                        U32_REG_WRITE(ptr_diepint, clear_sts.d32);
                    }

                    if (diepint0.b.txfe) {
                        DF_DEBUG_NEXT_EVENT(USB_DEVICE_DATA_IN_TX_FIFO_EMPTY_INTR);
                        stm32f4xx_usb_dtxfsts0_3_t dtxfsts = {0};
                        dtxfsts.d32 = U32_REG_READ(ptr_dtxfsts);
                        usb_device_endpoint_descriptor_t * ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
                        uint32_t word;
                        uint16_t num_bytes = ep->datain_byte_stream.cbuf_get_num_elem();
                        usb_assert(num_bytes);
                        if (dtxfsts.b.ineptfsav*4 > num_bytes) {
                            DF_DEBUG_NEXT_EVENT(USB_DEVICE_DATA_IN_TX_FIFO_WRITE);
                            for (uint16_t i=0; i<(num_bytes+3)/4; i++) {
                                ep->datain_byte_stream.cbuf_get_all((char*)&word, sizeof(word));
                                U32_REG_WRITE(ptr_dfifo, word);
                            }
                        } else {
                            usb_assert(0);
                        }

                        if (ep->datain_byte_stream.cbuf_get_num_elem()==0) {
                            /* Mask the fifo empty interrupt */
                            stm32f4xx_usb_diepempmsk_t diepempmsk = {0};
                            diepempmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPEMPMSK);
                            diepempmsk.b.ineptxfem &= ~(1<<ep_num);
                            U32_REG_WRITE(REG_USB_OTG_FS_DIEPEMPMSK, diepempmsk.d32);

                            stm32f4xx_usb_diepmsk0_3_t diepmsk = {0};
                            diepmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPMSK);
                            diepmsk.b.txfe = 0;
                            U32_REG_WRITE(REG_USB_OTG_FS_DIEPMSK, diepmsk.d32);
                        } else {
                            usb_assert(0);
                        }

                        /* Clear the interrupt bit */
                        diepint0.b.txfe = 0;
                    }

                    if (diepint0.b.xfrc) {
                        DF_DEBUG_NEXT_EVENT(USBD_ORIG_DATAINSTAGE_CALL);
                        /* Clear the interrupt bit */
                        diepint0.b.xfrc = 0;
                        stm32f4xx_usb_diepmsk0_3_t clear_sts = {0};
                        clear_sts.b.xfrc = 1;
                        U32_REG_WRITE(ptr_diepint, clear_sts.d32);

                        g_usb_device_handle->usb_device_handle_ep_datain_complete(ep_num);
                    }

                    if (diepint0.d32) {
                        usb_assert(0);
                    }
                }
                iepint >>= 1;
                ep_num++;
            }
            /* Clear the interrupt bit */
            gintr_status.b.iepint = 0;
        }

        if (gintr_status.b.sof) {
            /* Clear the reset bit */
            gintr_status.b.sof = 0;
            stm32f4xx_usb_gintsts_t clear_sts = {0};
            clear_sts.b.sof = 1;
            U32_REG_WRITE(REG_USB_OTG_FS_GINTSTS, clear_sts.d32);

            g_usb_device_handle->usb_device_handle_sof();
        }

        if (gintr_status.b.rxflvl) {    /* For data-out */
            DF_DEBUG_NEXT_EVENT(USBD_RXSTSQLVL_INTR);
            /* Disable the rx-fifo non-empty interrupt temporarily */
            stm32f4xx_usb_gintmsk_t gintmsk = {0};
            gintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_GINTMSK);
            gintmsk.b.rxflvl = 0;
            U32_REG_WRITE(REG_USB_OTG_FS_GINTMSK, gintmsk.d32);
            {
                uint8_t ep_num; 
                
                usb_device_endpoint_descriptor_t * ep;
                stm32f4xx_usb_grxstsp_t grxstsp = {0};
                stm32f4xx_usb_gintsts_t gintrsts = {0};
                grxstsp.d32 = U32_REG_READ(REG_USB_OTG_FS_GRXSTSP);
                ep_num = grxstsp.b.epnum;
                ep = g_usb_device_handle->usb_device_get_ep_descriptor(ep_num);
                usb_assert(ep);
                if (grxstsp.b.bcnt) {
                    switch (grxstsp.b.pktsts) {
                        case USB_SETUP_PACKET_RECEIVED:
                            usb_assert(grxstsp.b.bcnt==0x08);
                            DF_DEBUG_NEXT_EVENT(USBD_ORIG_SETUPPACKETRECEIVED);
                        case USB_OUT_DATA_PACKET_RECEIVED:
                            {
                                uint32_t word;
                                uint16_t num_words = (grxstsp.b.bcnt + 3)/4, num_bytes_filled;
                                for (uint16_t idx=0; idx<num_words; idx++) {
                                    word = U32_REG_READ(REG_USB_OTG_FS_DFIFO0);
                                    num_bytes_filled = ep->dataout_byte_stream.cbuf_put_all((char*)&word, sizeof(word));
                                    usb_assert(num_bytes_filled==sizeof(word));
                                }
                            }
                            break;
                        default:
                            usb_assert(0);
                    }
                } else {
                    DF_DEBUG_NEXT_EVENT(USBD_ORIG_RECEIVE_ZERO_BYTE_PACKET);
                    switch (grxstsp.b.pktsts) {
                        case USB_SETUP_TRANSACTION_COMPLETED:
                        case USB_OUT_DATA_PACKET_RECEIVED:
                        case USB_OUT_TRANSFER_COMPLETED:
                            break;
                        case USB_GLOBAL_OUT_NAK:
                        default:
                            usb_assert(0);
                    }
                }
            }
            /* Clear the reset bit */
            gintr_status.b.rxflvl = 0;

            /* Enable the rx-fifo non-empty interrupt temporarily */
            gintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_GINTMSK);
            gintmsk.b.rxflvl = 1;
            U32_REG_WRITE(REG_USB_OTG_FS_GINTMSK, gintmsk.d32);
        }

        if (gintr_status.b.usbrst) {
            DF_DEBUG_NEXT_EVENT(USBD_USBRESET_INTR);
            {
                stm32f4xx_usb_dctl_t dctl = {0};
                dctl.b.rwusig = 1;
                dctl.b.cginak = 1;
                dctl.b.cgonak = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_DCTL, dctl.d32);

                stm32f4xx_usb_grstctl_t grstctl = {0};
                grstctl.b.txfflsh = 1;
                grstctl.b.txfnum = 0x10;    /* All tx fifo's */
                U32_REG_WRITE(REG_USB_OTG_FS_GRSTCTL, grstctl.d32);
                do {
                   grstctl.d32 = U32_REG_READ(REG_USB_OTG_FS_GRSTCTL); 
                   stm32f4xx_usleep(10);
                } while (grstctl.b.txfflsh==1);

                U32_REG_WRITE(REG_USB_OTG_FS_DIEPINT0, 0xFFFFFFFF);
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPINT0, 0xFFFFFFFF);

                U32_REG_WRITE(REG_USB_OTG_FS_DAINT, 0xFFFFFFFF);
                stm32f4xx_usb_daintmsk_t daintmsk = {0};
                daintmsk.b.iepint |= 0x01;
                daintmsk.b.oepint |= 0x01;
                U32_REG_WRITE(REG_USB_OTG_FS_DAINTMSK, daintmsk.d32);

                stm32f4xx_usb_doepmsk0_3_t doepmsk0 = {0};
                doepmsk0.b.stup = 1;
                doepmsk0.b.xfrc = 1;
                doepmsk0.b.epdisd = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_DOEPMSK, doepmsk0.d32);

                stm32f4xx_usb_diepmsk0_3_t diepmsk0 = {0};
                diepmsk0.b.xfrc = 1;
                diepmsk0.b.epdisd = 1;
                diepmsk0.b.txfe = 1;
                diepmsk0.b.ittxfe = 0;
                U32_REG_WRITE(REG_USB_OTG_FS_DIEPMSK, diepmsk0.d32);

                stm32f4xx_usb_dcfg_t dcfg = {0};
                dcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_DCFG);
                dcfg.b.dad = 0x00;
                U32_REG_WRITE(REG_USB_OTG_FS_DCFG, dcfg.d32);

                g_usb_device_handle->usb_device_reset();
            }
            /* Clear the reset bit */
            gintr_status.b.usbrst = 0;
            stm32f4xx_usb_gintsts_t clear_sts = {0};
            clear_sts.b.usbrst = 1;
            U32_REG_WRITE(REG_USB_OTG_FS_GINTSTS, clear_sts.d32);

            stm32f4xx_usb_device_out_start(0, 64);
        }

        if (gintr_status.b.enumdone) {
            DF_DEBUG_NEXT_EVENT(USBD_ENUMDONE_INTR);
            {
                stm32f4xx_usb_dctl_t dctl = {0};
                dctl.b.cginak = 1;
                dctl.b.cgonak = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_DCTL, dctl.d32);

                stm32f4xx_usb_gusbcfg_t gusbcfg = {0};
                gusbcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_GUSBCFG);
                gusbcfg.b.toutcal = 5;
                U32_REG_WRITE(REG_USB_OTG_FS_GUSBCFG, gusbcfg.d32);

                /* Enable the endpoint interrupts */
                stm32f4xx_usb_gintmsk_t gintmsk = {0};
                gintmsk.d32 = U32_REG_READ(REG_USB_OTG_FS_GINTMSK);
                gintmsk.b.iepint = 1;
                gintmsk.b.oepint = 1;
                U32_REG_WRITE(REG_USB_OTG_FS_GINTMSK, gintmsk.d32);

                U32_REG_WRITE(REG_USB_OTG_FS_DIEPINT0, 0xFFFFFFFF);

            }
            /* Clear the enumdone bit */
            gintr_status.b.enumdone = 0;
            stm32f4xx_usb_gintsts_t clear_sts = {0};
            clear_sts.b.enumdone = 1;
            U32_REG_WRITE(REG_USB_OTG_FS_GINTSTS, clear_sts.d32);
        }

        if (gintr_status.d32) {
            usb_assert(0);
        }
    }
}

void stm32f4xx_usb_init(void)
{
    g_usb_device_hardware_handlers.schedule_data_in = stm32f4xx_usb_device_schedule_data_in;
    g_usb_device_hardware_handlers.schedule_data_out = stm32f4xx_usb_device_schedule_data_out;
    g_usb_device_hardware_handlers.set_address = stm32f4xx_usb_device_set_address;
    g_usb_device_hardware_handlers.activate_dataout = stm32f4xx_usb_device_activate_dataout;

    g_usb_device_state_handlers.configured = usb_comm_handler_intf_start;

    g_usb_comm_handle = usb_comm_t::get_instance();
    g_usb_comm_handle->usb_comm_init();

    g_usb_device_core_handlers.ep_datain_complete = usb_comm_handler_ep_datain_complete;
    g_usb_device_core_handlers.ep_dataout_complete = usb_comm_handler_ep_dataout_complete;

    g_usb_device_handle = usb_device_handle_t::get_instance();
    g_usb_device_handle->usb_device_init();

    stm32f4xx_usb_populate_device_descriptors(&g_usb_device_descriptor);
    stm32f4xx_usb_populate_endpoint_descriptor(&g_usb_device_endpoints[0], 0, USB_DEVICE_ENDPOINT_CONTROL, 2, 64);
    stm32f4xx_usb_populate_endpoint_descriptor(&g_usb_device_endpoints[1], 1, USB_DEVICE_ENDPOINT_BULK, 2, 64);
    stm32f4xx_usb_populate_interface_descriptor(&g_usb_interface[0], g_usb_device_endpoints, 2);
    stm32f4xx_usb_populate_configuration_descriptor(&g_usb_configuration, &g_usb_interface[0], 1);
    stm32f4xx_usb_populate_string_descriptor_zero(&g_usb_device_str_descriptor0);
    stm32f4xx_usb_populate_string_descriptors(g_usb_device_str_descriptors, &g_usb_device_num_str_descriptors);

    g_usb_device_handle->usb_device_setup_descriptors(&g_usb_device_descriptor, &g_usb_configuration, 
        1, &g_usb_device_endpoints[0],
        &g_usb_device_str_descriptor0, g_usb_device_str_descriptors, g_usb_device_num_str_descriptors);
    g_usb_device_handle->usb_device_setup_hardware_handlers(&g_usb_device_hardware_handlers);
    g_usb_device_handle->usb_device_setup_core_handlers(&g_usb_device_core_handlers);
    g_usb_device_handle->usb_device_setup_state_handlers(&g_usb_device_state_handlers);

    USB_OTG_BSP_Init(NULL); /* NVIC and GPIO Alternate-function setup */

    {
        /* Disable toplevel global interrupt */
        stm32f4xx_usb_gahbcfg_t gahbcfg = {0};
        gahbcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_GAHBCFG);
        gahbcfg.b.gintmsk = 0;
        U32_REG_WRITE(REG_USB_OTG_FS_GAHBCFG, gahbcfg.d32);
    }

    {
        /* Select the phy to use */
        stm32f4xx_usb_gusbcfg_t gusbcfg = {0};
        gusbcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_GUSBCFG);
        gusbcfg.b.physel = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GUSBCFG, gusbcfg.d32);
    }

    {
        /* Wait for ahb idle and soft reset the core */
        stm32f4xx_usb_grstctl_t grstctl = {0};
        do {
            stm32f4xx_usleep(3);
            grstctl.d32 = U32_REG_READ(REG_USB_OTG_FS_GRSTCTL);
        } while (grstctl.b.ahbidl==0);
    
        grstctl.b.csrst = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GRSTCTL, grstctl.d32);
        do {
            stm32f4xx_usleep(3);
            grstctl.d32 = U32_REG_READ(REG_USB_OTG_FS_GRSTCTL);
        } while (grstctl.b.csrst==1);
        stm32f4xx_msleep(30);
    }

    {
        /* Power up the USB core */
        stm32f4xx_usb_gccfg_t gccfg = {0};
        gccfg.b.pwrdwn = 1;
        gccfg.b.vbussasen = 1;
        gccfg.b.vbussbsen = 1;
        gccfg.b.novbussens = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GCCFG, gccfg.d32);
        stm32f4xx_msleep(20);
    }

    {
        /* Select the device mode */
        stm32f4xx_usb_gusbcfg_t gusbcfg = {0};
        gusbcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_GUSBCFG);
        gusbcfg.b.fhmod = 0;
        gusbcfg.b.fdmod = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GUSBCFG, gusbcfg.d32);
        stm32f4xx_msleep(50);
    }

    {
        /* Restart the Phy Clock */
        stm32f4xx_usb_pcgcctl_t pcgcctl = {0};
        U32_REG_WRITE(REG_USB_OTG_FS_PCGCCTL, pcgcctl.d32);
    }

    {
        /* Update the usb intervals and speed */
        stm32f4xx_usb_dcfg_t dcfg = {0};
        dcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_DCFG);
        dcfg.b.pfivl = 0;
        dcfg.b.dspd = 0x03;
        U32_REG_WRITE(REG_USB_OTG_FS_DCFG, dcfg.d32);
    }

    {
        /* Setup fifo space */
        stm32f4xx_usb_grxfsiz_t grxfsiz = {0};
        grxfsiz.b.rxfd = STM32F4XX_RX_FIFO_DEPTH;
        U32_REG_WRITE(REG_USB_OTG_FS_GRXFSIZ, grxfsiz.d32);

        stm32f4xx_usb_dieptxf0_t dieptxf0 = {0};
        dieptxf0.b.nptxfsa_tx0fsa = STM32F4XX_RX_FIFO_DEPTH*1;
        dieptxf0.b.nptxfd_tx0fd = STM32F4XX_RX_FIFO_DEPTH;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTXF0, dieptxf0.d32);
        
        stm32f4xx_usb_dieptxf1_3_t dieptxf1 = {0};
        dieptxf1.b.ineptxsa = STM32F4XX_RX_FIFO_DEPTH*2;
        dieptxf1.b.ineptxfd = STM32F4XX_RX_FIFO_DEPTH;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTXF1, dieptxf1.d32);

        stm32f4xx_usb_dieptxf1_3_t dieptxf2 = {0};
        dieptxf2.b.ineptxsa = STM32F4XX_RX_FIFO_DEPTH*3;
        dieptxf2.b.ineptxfd = STM32F4XX_RX_FIFO_DEPTH;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTXF2, dieptxf2.d32);

        stm32f4xx_usb_dieptxf1_3_t dieptxf3 = {0};
        dieptxf3.b.ineptxsa = STM32F4XX_RX_FIFO_DEPTH*4;
        dieptxf3.b.ineptxfd = STM32F4XX_RX_FIFO_DEPTH;
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTXF3, dieptxf3.d32);

        /* TODO- setup fifo depths for other endpoints */
    }
    
    {
        /* Flush all */
        stm32f4xx_usb_grstctl_t grstctl = {0};
        grstctl.b.txfflsh = 1;
        grstctl.b.txfnum = 0x10;    /* All tx fifo's */
        U32_REG_WRITE(REG_USB_OTG_FS_GRSTCTL, grstctl.d32);
        do {
           grstctl.d32 = U32_REG_READ(REG_USB_OTG_FS_GRSTCTL); 
           stm32f4xx_usleep(10);
        } while (grstctl.b.txfflsh==1);
        
        grstctl.d32 = 0;
        grstctl.b.rxfflsh = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GRSTCTL, grstctl.d32);
        do {
           grstctl.d32 = U32_REG_READ(REG_USB_OTG_FS_GRSTCTL); 
           stm32f4xx_usleep(10);
        } while (grstctl.b.rxfflsh==1);
    }

    {
        /* Disable all interrupts and reset any pending flags */
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPMSK, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_DOEPMSK, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_DAINTMSK, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_DAINT, 0xFFFFFFFF);

        stm32f4xx_usb_diepctl0_t diepctl0 = {0};
        diepctl0.d32 = U32_REG_READ(REG_USB_OTG_FS_DIEPCTL0);
        if (diepctl0.b.epena) {
            diepctl0.d32 = 0;
            diepctl0.b.epdis = 1;
            diepctl0.b.snak = 1;
        } else {
            diepctl0.d32 = 0;
        }
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPCTL0, diepctl0.d32);
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPTSIZ0, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_DIEPINT0, 0xFF);

        stm32f4xx_usb_doepctl0_t doepctl0 = {0};
        doepctl0.d32 = U32_REG_READ(REG_USB_OTG_FS_DOEPCTL0);
        if (doepctl0.b.epena) {
            doepctl0.d32 = 0;
            doepctl0.b.epdis = 1;
            doepctl0.b.snak = 1;
        } else {
            doepctl0.d32 = 0;
        }
        U32_REG_WRITE(REG_USB_OTG_FS_DOEPCTL0, doepctl0.d32);
        U32_REG_WRITE(REG_USB_OTG_FS_DOEPTSIZ0, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_DOEPINT0, 0xFF);

        U32_REG_WRITE(REG_USB_OTG_FS_GINTMSK, 0x00);
        U32_REG_WRITE(REG_USB_OTG_FS_GINTSTS, 0xFFFFFFFF);
    }
    
    {
        U32_REG_WRITE(REG_USB_OTG_FS_GOTGINT, 0xFFFFFFFF);
    }

    {
        stm32f4xx_usb_gintmsk_t gintmsk = {0};
        gintmsk.b.mmis = 1;
        gintmsk.b.sof = 1;
        gintmsk.b.rxflvl = 1;
        gintmsk.b.enumdone = 1;
        gintmsk.b.usbrst = 1;
        gintmsk.b.iepint = 1;
        gintmsk.b.oepint = 1;
        gintmsk.b.ginakeff = 1;
        gintmsk.b.goutnakeff = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GINTMSK, gintmsk.d32);

        stm32f4xx_usb_gahbcfg_t gahbcfg = {0};
        gahbcfg.d32 = U32_REG_READ(REG_USB_OTG_FS_GAHBCFG);
        gahbcfg.b.gintmsk = 1;
        gahbcfg.b.txfelvl = 1;
        U32_REG_WRITE(REG_USB_OTG_FS_GAHBCFG, gahbcfg.d32);
    }

    USB_OTG_BSP_EnableInterrupt(NULL);
    return;
}
