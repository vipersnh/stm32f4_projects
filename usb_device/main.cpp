#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "main.h"
    #include "FreeRTOS.h"
    #include "task.h"
}

#include "df_base.h"
#include "usb_core_common.hpp"
#include "cbuf_char.h"
#include "usb_device.h"
#include "stm32f4xx_usb.hpp"

#include "usb_comm_intf_device_ut.hpp"

void assert_led(void)
{
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
}

// Private variables
volatile uint32_t time_var1, time_var2;
void task_1(void *p)
{
    while(1) {
        GPIO_SetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1000);
        GPIO_ResetBits(GPIOD, GPIO_Pin_12);
        vTaskDelay(1000);
    }
}

void task_2_usb_tx_flush(void *p)
{
    char c[64];
    char s = 'a';
    unsigned int cnt;
    while (1) {
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        vTaskDelay(500);
        GPIO_ResetBits(GPIOD, GPIO_Pin_13);
        vTaskDelay(500);
    }
}

extern "C" {

void df_init_timer(void)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseInitTypeDef TIM_InitStruct;
    TIM_InitStruct.TIM_Prescaler = 84 - 1;                // This will configure the clock to 1 MHz
    TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;     // Count-up timer mode
    TIM_InitStruct.TIM_Period = 0xFFFFFFFF;                    // Auto reload
    TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;        // Divide clock by 1
    TIM_InitStruct.TIM_RepetitionCounter = 0;                // Set to 0, not used
    TIM_TimeBaseInit(TIM2, &TIM_InitStruct);
    /* TIM2 enable counter */
    TIM_Cmd(TIM2, ENABLE);
}

uint32_t df_get_timer_count(void)
{
    return TIM_GetCounter(TIM2);
}

}

void init(void) {
    SystemInit();
    df_init();
	GPIO_InitTypeDef  GPIO_InitStructure;
	// GPIOD Periph clock enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	// Configure PD12, PD13, PD14 and PD15 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
    extern void assert_led(void);
    g_usb_assert_callback = assert_led;
    usb_device_initialize();
}


int main() {
	init();
    xTaskCreate(usb_comm_intf_ut_task, (char *)"usb_comm_intf_ut_task", 100,0 ,tskIDLE_PRIORITY+2, 0);
    xTaskCreate(task_1, (char *)"task_1", 100,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_2_usb_tx_flush, (char *)"task_2_usb_tx_flush", 100,0 ,tskIDLE_PRIORITY+1, 0);
    vTaskStartScheduler();
	return -1;
}
