#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "tasks_demo.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "led.h"

QueueHandle_t   g_task_1_queue_handle;
QueueHandle_t   g_task_2_queue_handle;
QueueHandle_t   g_task_3_queue_handle;
QueueHandle_t   g_task_4_queue_handle;


void vCallbackFunction( TimerHandle_t xTimer ) {
    unsigned int x;
    print_to_usb("Inside timer callback vCallbackFunction\n");
    for (x=0; x<100; x++) {
        led_blue_on();
        vTaskDelay(10);
        led_blue_off();
        vTaskDelay(10);
    }
}


void task_1(void *p) {
    unsigned int value;
    while (1) {
        xQueueReceive(g_task_1_queue_handle, &value, portMAX_DELAY);
        print_to_usb("\nReceived message on task_1 queue\n");
        vTaskDelay(1000);
        led_green_on();
        vTaskDelay(1000);
        led_green_off();
        vTaskDelay(1000);
        print_to_usb("Sending message on task_2 queue\n");
        xQueueSend(g_task_2_queue_handle, (void *) 0x01, 0);
    }
}

void task_2(void *p) {
    unsigned int value;
    value = 0x00;
    xQueueSend(g_task_1_queue_handle, (void *) &value, 0);
    while (1) {
        xQueueReceive(g_task_2_queue_handle, &value, portMAX_DELAY);
        print_to_usb("\nReceived message on task_2 queue\n");
        vTaskDelay(1000);
        led_orange_on();
        vTaskDelay(1000);
        led_orange_off();
        vTaskDelay(1000);
        print_to_usb("Sending message on task_3 queue\n");
        value = 0x02;
        xQueueSend(g_task_3_queue_handle, (void *) &value, 0);
    }
}

void task_3(void *p) {
    unsigned int value;
    while (1) {
        xQueueReceive(g_task_3_queue_handle, &value, portMAX_DELAY);
        print_to_usb("\nReceived message on task_3 queue\n");
        vTaskDelay(1000);
        led_green_on();
        vTaskDelay(1000);
        led_green_off();
        vTaskDelay(1000);
        print_to_usb("Sending message on task_4 queue\n");
        value = 0x03;
        xQueueSend(g_task_4_queue_handle, (void *) &value, 0);
    }
}

void task_4(void *p) {
    unsigned int value;
    while (1) {
        xQueueReceive(g_task_4_queue_handle, &value, portMAX_DELAY);
        print_to_usb("\nReceived message on task_4 queue\n");
        vTaskDelay(1000);
        led_blue_on();
        vTaskDelay(1000);
        led_blue_off();
        vTaskDelay(1000);
        print_to_usb("Sending message on task_1 queue\n");
        value = 0x04;
        xQueueSend(g_task_1_queue_handle, (void *) &value, 0);
    }
}

void freertos_start() {
    xTaskCreate(task_1, (char *)"task_1", 100,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_2, (char *)"task_2", 100,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_3, (char *)"task_3", 100,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_4, (char *)"task_4", 100,0 ,tskIDLE_PRIORITY+1, 0);
    g_task_1_queue_handle = xQueueCreate(5, 2);
    g_task_2_queue_handle = xQueueCreate(5, 2);
    g_task_3_queue_handle = xQueueCreate(5, 2);
    g_task_4_queue_handle = xQueueCreate(5, 2);

    vTaskStartScheduler();
}


