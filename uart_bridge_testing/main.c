#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "usbd_cdc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_cdc_vcp.h"
#include "usart.h"
#include "stm32f401_discovery.h"
#include "stm32f401_discovery_accelerometer.h"
#include "stm32f401_discovery_gyroscope.h"

// Private variables
volatile uint32_t time_var1, time_var2;
__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;

int _getpid(void) {
  return 1;
}

void _kill(int pid) { while(1) ; }

void usart_to_usb(char c)
{
    VCP_put_char((uint8_t)c);
}

void usb_to_usart(char c)
{
    USART2_puts(c);
}

void print_to_usb(char *c) {
    unsigned int len = strlen(c), i;
    for (i=0; i<len; i++) {
        usart_to_usb(c[i]);
    }
}

void init(void) {
    SystemInit();
	GPIO_InitTypeDef  GPIO_InitStructure;
	// GPIOD Periph clock enable
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	// Configure PD12, PD13, PD14 and PD15 in output pushpull mode
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	// ------------- USB -------------- //
	USBD_Init(&USB_OTG_dev,
	            USB_OTG_FS_CORE_ID,
	            &USR_desc,
	            &USBD_CDC_cb,
	            &USR_cb);
    init_USART2(115200, &usart_to_usb);
}


int main(void) {
	init();
	/*
	 * Disable STDOUT buffering. Otherwise nothing will be printed
	 * before a newline character or when the buffer is flushed.
	 */
    extern void freertos_start();
    freertos_start();
    while (1);
	return -1;
}


