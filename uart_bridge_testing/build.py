#!python
import sys
sys.path.append("/hdd1/data/projects/vipersnh_git/python_library/make")
sys.path.insert(0, '../common')

import make
import build_common

profile_main = make.profile_t()

include_dirs = ["./", "./common", "./device", "../../drivers/usb_device/device"] + build_common.profile.include_dirs
source_dirs = build_common.profile.source_dirs
source_files = build_common.profile.source_files + [
    "main.c", 
    "tasks_demo.c",
    "led.c",
    "system_stm32f4xx.c", 
    "stm32f4xx_it.c",
    "usbd_desc.c", 
    "usbd_usr.c", 
    "usbd_cdc_vcp.c", 
    "syscalls.c",
    "usart.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_Device/Class/cdc/src/usbd_cdc_core.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_Device/Core/src/usbd_core.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_Device/Core/src/usbd_req.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_Device/Core/src/usbd_ioreq.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_OTG/src/usb_core.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_OTG/src/usb_dcd.c",
    "/hdd1/data/projects/vipersnh_git/stm32f4_libs/USB_OTG/src/usb_dcd_int.c"]

default_c_cpp_flags = ["-DSTM32F4XX_USB_DRIVER=1", "-enum_is_int"]

cflags = build_common.profile.cflags + default_c_cpp_flags
cxxflags = build_common.profile.cxxflags + default_c_cpp_flags
ldflags =  ["--static"] + build_common.profile.ldflags
linker_scripts = build_common.profile.linker_scripts
libraries = ["-lm", "-lc"]
arflags = list()

profile_main.name = "main"
profile_main.cc = "arm-none-eabi-gcc"
profile_main.cc_asm = "arm-none-eabi-as"
profile_main.cxx = "arm-none-eabi-g++"
profile_main.ld  = "arm-none-eabi-g++"
profile_main.objcopy = "arm-none-eabi-objcopy"
profile_main.objdump = "arm-none-eabi-objdump"
profile_main.include_dirs = include_dirs
profile_main.source_dirs = source_dirs
profile_main.source_files = source_files
profile_main.cflags = cflags
profile_main.cxxflags = cxxflags
profile_main.ldflags = ldflags
profile_main.linker_scripts = linker_scripts
profile_main.libraries = libraries
profile_main.arflags = arflags
profile_main.executable = "main.elf"
profile_main.library = None
profile_main.verbose_level = make.verbosity.MINIMAL
profile_main.build_dir = "./build"
profile_main.executable_subtargets["iHex"] = [profile_main.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.hex"]
profile_main.executable_subtargets["binary"] = [profile_main.objcopy, "-O", "binary", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.bin"]
profile_main.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_main) + "main.bin",
    "0x8000000"]



profiles = [profile_main]

make.build(profiles_list = profiles, args = sys.argv[1:])
