#ifndef _USART_H_
#define _USART_H_

typedef void (*callback_fn_ptr)(char c);

void init_USART2(uint32_t baudrate, callback_fn_ptr callback);
void USART2_puts(char c);



#endif
