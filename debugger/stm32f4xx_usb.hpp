#ifndef __STM32F4XX_USB_H_
#define __STM32F4XX_USB_H_

#define stm32f4xx_usleep(_x)  USB_OTG_BSP_uDelay(_x)
#define stm32f4xx_msleep(_x)  USB_OTG_BSP_mDelay(_x)
#define stm32f4xx_sleep(_x)   USB_OTG_BSP_mDelay(_x*1000)

#define STM32F4XX_RX_FIFO_DEPTH 64

#define U32_REG_WRITE(_reg, _value) (*(volatile uint32_t*)(_reg)) = (_value)
#define U32_REG_READ(_reg) (*(volatile uint32_t*)(_reg))
#define U32_REG_MODIFY(_reg, _value, _mask) (*(volatile uint32_t *)(_reg)) = (((*(volatile uint32_t *)(_reg)) & _mask ) | _value)

#define REG_USB_OTG_FS_BASE     (0x50000000)
#define REG_USB_OTG_FS_GOTGINT  (REG_USB_OTG_FS_BASE + 0x004)
#define REG_USB_OTG_FS_GAHBCFG  (REG_USB_OTG_FS_BASE + 0x008)
#define REG_USB_OTG_FS_GUSBCFG  (REG_USB_OTG_FS_BASE + 0x00C)
#define REG_USB_OTG_FS_GRSTCTL  (REG_USB_OTG_FS_BASE + 0x010)
#define REG_USB_OTG_FS_GINTSTS  (REG_USB_OTG_FS_BASE + 0x014)
#define REG_USB_OTG_FS_GINTMSK  (REG_USB_OTG_FS_BASE + 0x018)

#define REG_USB_OTG_FS_GRXSTSR  (REG_USB_OTG_FS_BASE + 0x01C)
#define REG_USB_OTG_FS_GRXSTSP  (REG_USB_OTG_FS_BASE + 0x020)

#define REG_USB_OTG_FS_GRXFSIZ  (REG_USB_OTG_FS_BASE + 0x024)
#define REG_USB_OTG_FS_GCCFG    (REG_USB_OTG_FS_BASE + 0x038)
#define REG_USB_OTG_FS_CID      (REG_USB_OTG_FS_BASE + 0x03C)
#define REG_USB_OTG_FS_DCFG     (REG_USB_OTG_FS_BASE + 0x800)
#define REG_USB_OTG_FS_DCTL     (REG_USB_OTG_FS_BASE + 0x804)
#define REG_USB_OTG_FS_DSTS     (REG_USB_OTG_FS_BASE + 0x808)
#define REG_USB_OTG_FS_DIEPMSK  (REG_USB_OTG_FS_BASE + 0x810)
#define REG_USB_OTG_FS_DOEPMSK  (REG_USB_OTG_FS_BASE + 0x814)
#define REG_USB_OTG_FS_DAINT    (REG_USB_OTG_FS_BASE + 0x818)
#define REG_USB_OTG_FS_DAINTMSK (REG_USB_OTG_FS_BASE + 0x81C)
#define REG_USB_OTG_FS_DIEPEMPMSK (REG_USB_OTG_FS_BASE + 0x834)
#define REG_USB_OTG_FS_DIEPCTL0 (REG_USB_OTG_FS_BASE + 0x900 + 0x20*0)
#define REG_USB_OTG_FS_DIEPCTL1 (REG_USB_OTG_FS_BASE + 0x900 + 0x20*1)
#define REG_USB_OTG_FS_DIEPCTL2 (REG_USB_OTG_FS_BASE + 0x900 + 0x20*2)
#define REG_USB_OTG_FS_DIEPCTL3 (REG_USB_OTG_FS_BASE + 0x900 + 0x20*3)

#define REG_USB_OTG_FS_DOEPCTL0 (REG_USB_OTG_FS_BASE + 0xB00 + 0x20*0)
#define REG_USB_OTG_FS_DOEPCTL1 (REG_USB_OTG_FS_BASE + 0xB00 + 0x20*1)
#define REG_USB_OTG_FS_DOEPCTL2 (REG_USB_OTG_FS_BASE + 0xB00 + 0x20*2)
#define REG_USB_OTG_FS_DOEPCTL3 (REG_USB_OTG_FS_BASE + 0xB00 + 0x20*3)

#define REG_USB_OTG_FS_DIEPINT0 (REG_USB_OTG_FS_BASE + 0x908 + 0x20*0) 
#define REG_USB_OTG_FS_DIEPINT1 (REG_USB_OTG_FS_BASE + 0x908 + 0x20*1)
#define REG_USB_OTG_FS_DIEPINT2 (REG_USB_OTG_FS_BASE + 0x908 + 0x20*2)
#define REG_USB_OTG_FS_DIEPINT3 (REG_USB_OTG_FS_BASE + 0x908 + 0x20*3)

#define REG_USB_OTG_FS_DOEPINT0 (REG_USB_OTG_FS_BASE + 0xB08 + 0x20*0)
#define REG_USB_OTG_FS_DOEPINT1 (REG_USB_OTG_FS_BASE + 0xB08 + 0x20*1)
#define REG_USB_OTG_FS_DOEPINT2 (REG_USB_OTG_FS_BASE + 0xB08 + 0x20*2)
#define REG_USB_OTG_FS_DOEPINT3 (REG_USB_OTG_FS_BASE + 0xB08 + 0x20*3)

#define REG_USB_OTG_FS_DIEPTSIZ0 (REG_USB_OTG_FS_BASE + 0x910 + 0x20*0)
#define REG_USB_OTG_FS_DIEPTSIZ1 (REG_USB_OTG_FS_BASE + 0x910 + 0x20*1)
#define REG_USB_OTG_FS_DIEPTSIZ2 (REG_USB_OTG_FS_BASE + 0x910 + 0x20*2)
#define REG_USB_OTG_FS_DIEPTSIZ3 (REG_USB_OTG_FS_BASE + 0x910 + 0x20*3)

#define REG_USB_OTG_FS_DOEPTSIZ0 (REG_USB_OTG_FS_BASE + 0xB10 + 0x20*0)
#define REG_USB_OTG_FS_DOEPTSIZ1 (REG_USB_OTG_FS_BASE + 0xB10 + 0x20*1)
#define REG_USB_OTG_FS_DOEPTSIZ2 (REG_USB_OTG_FS_BASE + 0xB10 + 0x20*2)
#define REG_USB_OTG_FS_DOEPTSIZ3 (REG_USB_OTG_FS_BASE + 0xB10 + 0x20*3)

#define REG_USB_OTG_FS_DTXFSTS0  (REG_USB_OTG_FS_BASE + 0x918 + 0x20*0)
#define REG_USB_OTG_FS_DTXFSTS1  (REG_USB_OTG_FS_BASE + 0x918 + 0x20*1)
#define REG_USB_OTG_FS_DTXFSTS2  (REG_USB_OTG_FS_BASE + 0x918 + 0x20*2)
#define REG_USB_OTG_FS_DTXFSTS3  (REG_USB_OTG_FS_BASE + 0x918 + 0x20*3)

#define REG_USB_OTG_FS_PCGCCTL   (REG_USB_OTG_FS_BASE + 0xE00)

#define REG_USB_OTG_FS_DIEPTXF0  (REG_USB_OTG_FS_BASE + 0x028)
#define REG_USB_OTG_FS_DIEPTXF1  (REG_USB_OTG_FS_BASE + 0x104)
#define REG_USB_OTG_FS_DIEPTXF2  (REG_USB_OTG_FS_BASE + 0x124)
#define REG_USB_OTG_FS_DIEPTXF3  (REG_USB_OTG_FS_BASE + 0x138)

#define REG_USB_OTG_FS_DFIFO0 (REG_USB_OTG_FS_BASE + 0x1000 + 0x1000*0)
#define REG_USB_OTG_FS_DFIFO1 (REG_USB_OTG_FS_BASE + 0x1000 + 0x1000*1)
#define REG_USB_OTG_FS_DFIFO2 (REG_USB_OTG_FS_BASE + 0x1000 + 0x1000*2)
#define REG_USB_OTG_FS_DFIFO3 (REG_USB_OTG_FS_BASE + 0x1000 + 0x1000*3)

typedef union {
    uint32_t d32;
    struct
    {
        uint32_t cmod : 1;
        uint32_t mmis : 1;
        uint32_t otgint : 1;
        uint32_t sof : 1;
        uint32_t rxflvl : 1;
        uint32_t nptxfe : 1;
        uint32_t ginakeff : 1;
        uint32_t goutnakeff : 1;
        uint32_t Reserved8_9 : 2;
        uint32_t esusp : 1;
        uint32_t usbsusp : 1;
        uint32_t usbrst : 1;
        uint32_t enumdone : 1;
        uint32_t isooutdrp : 1;
        uint32_t eopf : 1;
        uint32_t Reserved16_17 : 2;
        uint32_t iepint: 1;
        uint32_t oepint : 1;
        uint32_t iisoixfr : 1;
        uint32_t ipxfr_incommpisoout : 1;
        uint32_t Reserved22_23 : 2;
        uint32_t hprtint : 1;
        uint32_t hcint : 1;
        uint32_t ptxfe : 1;
        uint32_t Reserved27 : 1;
        uint32_t cidschg : 1;
        uint32_t discint : 1;
        uint32_t srqint : 1;
        uint32_t wkuint : 1;
    } b;
} stm32f4xx_usb_gintsts_t ;

typedef  stm32f4xx_usb_gintsts_t stm32f4xx_usb_gintmsk_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t gintmsk : 1;
        uint32_t Reserved1_6 : 6;
        uint32_t txfelvl : 1;
        uint32_t ptxfelvl : 1;
        uint32_t Reserved9_31 : 23;
    } b;
} stm32f4xx_usb_gahbcfg_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t toutcal : 3;
        uint32_t Reserved3_5 : 3;
        uint32_t physel : 1;
        uint32_t Reserved7 : 1;
        uint32_t srpcap : 1;
        uint32_t hnpcap : 1;
        uint32_t trdt : 4;
        uint32_t Reserved14_28 : 15;
        uint32_t fhmod : 1;
        uint32_t fdmod : 1;
        uint32_t ctxpkt : 1; 
    } b;
} stm32f4xx_usb_gusbcfg_t ;

typedef union {
    uint32_t d32;
    struct {
        uint32_t Reserved0_15 : 16;
        uint32_t pwrdwn : 1;
        uint32_t Reserved17 : 1;
        uint32_t vbussasen : 1;
        uint32_t vbussbsen : 1;
        uint32_t sofouten : 1;
        uint32_t novbussens : 1;
        uint32_t Reserved22_31 : 10;
    } b;
} stm32f4xx_usb_gccfg_t;


typedef union {
    uint32_t d32;
    struct {
        uint32_t csrst :1;
        uint32_t hsrst :1;
        uint32_t fcrst :1;
        uint32_t Reserved3 :1;
        uint32_t rxfflsh :1;
        uint32_t txfflsh :1;
        uint32_t txfnum :5;
        uint32_t Reserved11_30 :20;
        uint32_t ahbidl : 1;
    } b;
    
} stm32f4xx_usb_grstctl_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t mpsiz : 2;
        uint32_t Reserved2_14 : 13;
        uint32_t usbaep : 1;
        uint32_t Reserved16 : 1;
        uint32_t naksts : 1;
        uint32_t eptyp : 2;
        uint32_t Reserved20 : 1;
        uint32_t stall : 1;
        uint32_t txfnum : 4;
        uint32_t cnak : 1;
        uint32_t snak : 1;
        uint32_t Reserved28_29 : 2;
        uint32_t epdis : 1;
        uint32_t epena : 1;
    } b;
} stm32f4xx_usb_diepctl0_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t mpsiz : 11;
        uint32_t Reserved11_14 : 4;
        uint32_t usbaep : 1;
        uint32_t eonum_dpid : 1;
        uint32_t naksts : 1;
        uint32_t eptyp : 2;
        uint32_t Reserved20 : 1;
        uint32_t stall : 1;
        uint32_t txfnum : 4;
        uint32_t cnak : 1;
        uint32_t snak : 1;
        uint32_t sd0pid_sevnfrm : 1;
        uint32_t soddfrm : 1;
        uint32_t epdis : 1;
        uint32_t epena : 1;
    } b;
} stm32f4xx_usb_diepctl1_3_t;


typedef union {
    uint32_t d32;
    struct {
        uint32_t mpsiz : 2;
        uint32_t Reserved2_14 : 13;
        uint32_t usbaep : 1;
        uint32_t Reserved16 : 1;
        uint32_t naksts : 1;
        uint32_t eptyp : 2;
        uint32_t snpm :1;
        uint32_t stall :1;
        uint32_t Reserved22_25 : 4;
        uint32_t cnak :1;
        uint32_t snak :1;
        uint32_t Reserved28_29 :2;
        uint32_t epdis :1;
        uint32_t epena :1;
    } b;
} stm32f4xx_usb_doepctl0_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t mpsiz : 11;
        uint32_t Reserved11_14 :4;
        uint32_t usbaep :1;
        uint32_t eonum_dpid :1;
        uint32_t naksts :1;
        uint32_t eptyp :2;
        uint32_t snpm :1;
        uint32_t stall :1;
        uint32_t Reserved22_25 : 4;
        uint32_t cnak :1;
        uint32_t snak :1;
        uint32_t sd0pid_sevnfrm :1;
        uint32_t soddfrm_sd1pid :1;
        uint32_t epdis :1;
        uint32_t epena :1;
    } b;
} stm32f4xx_usb_doepctl1_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t dspd :2;
        uint32_t nzlsohsk :1;
        uint32_t Reserved3 : 1;
        uint32_t dad : 7;
        uint32_t pfivl : 2;
        uint32_t Reserved13_31: 19;
    } b;
} stm32f4xx_usb_dcfg_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t ineptxsa :16;
        uint32_t ineptxfd :16;
    } b;
} stm32f4xx_usb_dieptxf1_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrc :1;
        uint32_t epdisd :1;
        uint32_t Reserved2 :1;
        uint32_t toc :1;
        uint32_t ittxfe :1;
        uint32_t Reserved5 :1;
        uint32_t inepne :1;
        uint32_t txfe :1;
        uint32_t Reserved8_31 :24;
    } b;
} stm32f4xx_usb_diepint0_3_t;

typedef stm32f4xx_usb_diepint0_3_t stm32f4xx_usb_diepmsk0_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrc :1;
        uint32_t epdisd :1;
        uint32_t Reserved2 :1;
        uint32_t stup :1;
        uint32_t otepdis :1;
        uint32_t Reserved5 :1;
        uint32_t b2bstup :1;
        uint32_t Reserved7 :1;
        uint32_t Reserved8_31 :24;
    } b;
} stm32f4xx_usb_doepint0_3_t;

typedef stm32f4xx_usb_doepint0_3_t  stm32f4xx_usb_doepmsk0_3_t ;

typedef union {
    uint32_t d32;
    struct {
        uint32_t iepint :16;
        uint32_t oepint :16;
    } b;
} stm32f4xx_usb_daint_t;

typedef stm32f4xx_usb_daint_t stm32f4xx_usb_daintmsk_t;

typedef enum {
    USB_GLOBAL_OUT_NAK = 0b0001,
    USB_OUT_DATA_PACKET_RECEIVED = 0b0010,
    USB_OUT_TRANSFER_COMPLETED = 0b0011,
    USB_SETUP_TRANSACTION_COMPLETED = 0b0100,
    USB_SETUP_PACKET_RECEIVED = 0b0110,
} stm32f4xx_usb_grstctl_pktsts_enum_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t epnum :4;
        uint32_t bcnt :11;
        uint32_t dpid :2;
        uint32_t pktsts :4;
        uint32_t frmnum :4;
        uint32_t Reserved25_31 :7;
    } b;
} stm32f4xx_usb_grxstsp_t;

typedef stm32f4xx_usb_grxstsp_t stm32f4xx_usb_grxstsr_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t rxfd :16;
        uint32_t Reserved16_31 :16;
    } b;
} stm32f4xx_usb_grxfsiz_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t nptxfsa_tx0fsa :16;
        uint32_t nptxfd_tx0fd :16;
    } b;
} stm32f4xx_usb_dieptxf0_t;


typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrsiz : 7;
        uint32_t Reserved7_18 : 12;
        uint32_t pktcnt : 2;
        uint32_t Reserved21_31 : 11;
    } b;
} stm32f4xx_usb_dieptsiz0_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrsiz : 19;
        uint32_t pktcnt : 10;
        uint32_t mcnt : 2;
        uint32_t Reserved31 : 1;
    } b;
} stm32f4xx_usb_dieptsiz1_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrsiz : 7;
        uint32_t Reserved7_18 : 12;
        uint32_t pktcnt : 1;
        uint32_t Reserved20_28: 9;
        uint32_t stupcnt : 2;
        uint32_t Reserved31 : 1;
    } b;
} stm32f4xx_usb_doeptsiz0_t;


typedef union {
    uint32_t d32;
    struct {
        uint32_t xfrsiz : 19;
        uint32_t pktcnt : 10;
        uint32_t rxdpid_stupcnt : 2;
        uint32_t Reserved31 : 1;
    } b;
} stm32f4xx_usb_doeptsiz1_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t suspsts : 1;
        uint32_t enumspd : 2;
        uint32_t eerr : 1;
        uint32_t Reserved4_7 : 4;
        uint32_t fnsof : 14;
        uint32_t Reserved22_31 : 10;
    } b;
} stm32f4xx_usb_dsts_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t rwusig : 1;
        uint32_t sdis : 1;
        uint32_t ginsts : 1;
        uint32_t gonsts : 1;
        uint32_t tctl : 3;
        uint32_t sginak : 1;
        uint32_t cginak : 1;
        uint32_t sgonak : 1;
        uint32_t cgonak : 1;
        uint32_t poprgdne : 1;
        uint32_t Reserved12_31 : 20;
    } b;
} stm32f4xx_usb_dctl_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t ineptfsav : 16;
        uint32_t Reserved16_31 : 16;
    } b;
} stm32f4xx_usb_dtxfsts0_3_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t ineptxfem : 16;
        uint32_t Reserved16_31 : 16;
    } b;
} stm32f4xx_usb_diepempmsk_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t stppclk : 1;
        uint32_t gatehclk : 1;
        uint32_t Reserved2_3 : 2;
        uint32_t physusp : 1;
        uint32_t Reserved5_31 : 27;
    } b;
} stm32f4xx_usb_pcgcctl_t;


#ifdef __cplusplus
extern "C" { 
#endif
    void stm32f4xx_usb_isr(void);
    void stm32f4xx_usb_init(void);
#ifdef __cplusplus
}
#endif

#endif
