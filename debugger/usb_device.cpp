extern "C" {
    #include <stdint.h>
}
#include "cbuf_char.h"
#include "usb_device.h"
#include "stm32f4xx_usb.hpp"

void usb_device_initialize()
{
    stm32f4xx_usb_init();
    return;
}
