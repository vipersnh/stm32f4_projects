extern "C" {
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
}
#include "df_base.h"
#include "bitops.hpp"
#include "stm32f4xx_gpio.h"
#include "swdio_intf.hpp"
#include "swdio.hpp"

#define SWD_DEBUG_WORDS 100 

bool g_swd_debug_enable = false;
uint32_t g_swd_debug_bit_idx = 0;
uint32_t g_swd_debug_swdio_words[SWD_DEBUG_WORDS];
uint32_t g_swd_debug_swdclk_words[SWD_DEBUG_WORDS];

#define DF_DEBUG_STORE() { \
    if (g_swd_debug_enable) {\
        bool swdio_bit = GPIO_ReadInputDataBit(SWDIO_GPIO_PORT, SWDIO_GPIO_IO_PIN)==Bit_SET; \
        bool swdclk_bit = GPIO_ReadInputDataBit(SWDIO_GPIO_PORT, SWDIO_GPIO_CLK_PIN)==Bit_SET; \
        uint8_t wrd_idx = (g_swd_debug_bit_idx/32) % SWD_DEBUG_WORDS; \
        uint8_t bit_idx = (g_swd_debug_bit_idx%32); \
        g_swd_debug_swdio_words[wrd_idx] |= swdio_bit << bit_idx; \
        g_swd_debug_swdclk_words[wrd_idx] |= swdclk_bit << bit_idx; \
        g_swd_debug_bit_idx++; \
    } \
}

static swdio_t * g_swdio_handle = NULL;

swdio_t * swdio_t::instance;

static void swdio_delay_us(uint32_t us)
{
    uint32_t i = 84*us;
    do {
    } while(i--);
}

/* Class instantiation operations */
swdio_t::swdio_t(void)
{
    this->instance = NULL;
    this->num_trn_clks = SWDIO_DEFAULT_TRN_CLKS;
    this->last_resp    = SWD_ACK_OK;
    this->control_register.d32 = 0x00;
}

swdio_t * swdio_t::get_instance()
{
    if (instance==NULL) {
        instance = new swdio_t();
        instance->swdio_gpio_init();
        g_swdio_handle = instance;
    } else {
        /* TODO */
    }
    return instance;
}

void swdio_t::swdio_gpio_init()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitStructure.GPIO_Pin = SWDIO_GPIO_RST_PIN | SWDIO_GPIO_IO_PIN | SWDIO_GPIO_CLK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SWDIO_GPIO_PORT, &GPIO_InitStructure);
    GPIO_WriteBit(SWDIO_GPIO_PORT, SWDIO_GPIO_IO_PIN,  Bit_SET);
    GPIO_WriteBit(SWDIO_GPIO_PORT, SWDIO_GPIO_CLK_PIN, Bit_RESET);
    swdio_set_io_as_input();
    swdio_set_clk_as_output();
}

void swdio_t::swdio_set_io_as_output(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure = {0};
	GPIO_InitStructure.GPIO_Pin = SWDIO_GPIO_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(SWDIO_GPIO_PORT, &GPIO_InitStructure);
}

void swdio_t::swdio_set_io_as_input(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure = {0};
	GPIO_InitStructure.GPIO_Pin = SWDIO_GPIO_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SWDIO_GPIO_PORT, &GPIO_InitStructure);
}

void swdio_t::swdio_set_clk_as_output(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure = {0};
	GPIO_InitStructure.GPIO_Pin = SWDIO_GPIO_CLK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(SWDIO_GPIO_PORT, &GPIO_InitStructure);
}

void swdio_t::swdio_set_clk_as_input(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure = {0};
	GPIO_InitStructure.GPIO_Pin = SWDIO_GPIO_CLK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SWDIO_GPIO_PORT, &GPIO_InitStructure);
}

/* SWD high level abstraction operations */
swdio_resp_enum_t swdio_t::swdio_dp_connect(uint32_t * idcode)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    g_swd_debug_enable = true;
    g_swd_debug_bit_idx = 0;
    memset(g_swd_debug_swdio_words, 0x00, sizeof(g_swd_debug_swdio_words));
    memset(g_swd_debug_swdclk_words, 0x00, sizeof(g_swd_debug_swdclk_words));
    if (idcode) {
        swdio_bit_t bits[16];
        uint8_t bit_idx=0;
        do {
            bits[bit_idx] = ((1<<bit_idx) & SWDIO_JTAG_TO_SWD_SEQ)!=0;
            bit_idx++;
        } while (bit_idx<16);
        swdio_shift_reset();
        swdio_write_bits(bits, 16);
        swdio_line_reset();
        return swdio_dp_get_idcode(idcode);
    } else {
        status = SWD_SW_ERROR;
    }
}

swdio_resp_enum_t swdio_t::swdio_dp_get_idcode(uint32_t * idcode)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    if (idcode) {
         status = swdio_dp_read(SWD_DP_IDCODE_R_ABORT_W_REG, idcode);
    } else {
        status = SWD_SW_ERROR;
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_ap_select(uint8_t ap_sel, uint8_t bank_sel, bool ctrlSel)
{
    uint32_t value = BITOPS_SHIFTED_VALUE(ap_sel, 24, 8) | BITOPS_SHIFTED_VALUE(bank_sel, 4, 4) | 
        BITOPS_SHIFTED_VALUE(ctrlSel, 0, 1);
    return swdio_dp_write(SWD_DP_RD_RESEND_R_APSEL_W_REG, value);
}


swdio_resp_enum_t swdio_t::swdio_ap_get_idcode(uint8_t ap_idx, uint32_t * idcode)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    if (idcode) {
        status = swdio_ap_select(ap_idx, 0x00);  /* Select the AP-idx specified and 0th register bank */
        if (status==SWD_ACK_OK) {
            status = swdio_ap_read(SWD_AP_IDCODE_REG, idcode);
        }
    } else {
        status = SWD_SW_ERROR;
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_dp_read(uint8_t reg, uint32_t * value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    bool APnDP = 0;
    bool RnW   = 1;
    if (!SWDIO_DP_REG_VALIDATE(reg) || (value==NULL)) {
        status = SWD_SW_ERROR;
    } else {
        swdio_write_cmd(APnDP, RnW, reg);
        swdio_trn_gap();
        status = swdio_read_resp();
        if (status==SWD_ACK_OK) {
            swdio_parity_result_t parity_result;
            parity_result = swdio_read_data(value);
        } else {
            if (this->control_register.b.bit_ORUNDETECT) {
                swdio_read_data(value);
            }
        }
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_dp_write(uint8_t reg, uint32_t value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    bool APnDP = 0;
    bool RnW   = 0;
    if (!SWDIO_DP_REG_VALIDATE(reg)) {
        status = SWD_SW_ERROR;
    } else {
        if (reg==0x04) {
            /* Store the value written to control register */
            this->control_register.d32 = value;
        }
        swdio_write_cmd(APnDP, RnW, reg);
        swdio_trn_gap();
        status = swdio_read_resp();
        if (status==SWD_ACK_OK) {
            swdio_trn_gap();
            swdio_write_data(value);
        } else {
            if (this->control_register.b.bit_ORUNDETECT) {
                swdio_trn_gap();
                swdio_write_data(value);
            }
        }
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_ap_read(uint32_t reg, uint32_t * value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    bool APnDP = 1;
    bool RnW   = 1;
    if (!SWDIO_AP_REG_VALIDATE(reg) || (value==NULL)) {
        status = SWD_SW_ERROR;
    } else {
        swdio_write_cmd(APnDP, RnW, reg);
        swdio_trn_gap();
        status = swdio_read_resp();
        if (status==SWD_ACK_OK) {
            swdio_parity_result_t parity_result;
            parity_result = swdio_read_data(value);
        } else {
            if (this->control_register.b.bit_ORUNDETECT) {
                swdio_read_data(value);
            }
        }
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_ap_write(uint32_t reg, uint32_t value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    bool APnDP = 1;
    bool RnW   = 0;
    if (!SWDIO_AP_REG_VALIDATE(reg)) {
        status = SWD_SW_ERROR;
    } else {
        swdio_write_cmd(APnDP, RnW, reg);
        swdio_trn_gap();
        status = swdio_read_resp();
        if (status==SWD_ACK_OK) {
            swdio_trn_gap();
            swdio_write_data(value);
        } else {
            if (this->control_register.b.bit_ORUNDETECT) {
                swdio_trn_gap();
                swdio_write_data(value);
            }
        }
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_dp_read_status(uint32_t * value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    if (value) {
        swdio_dp_read(SWD_DP_CTRL_STAT_RW_WCR_RW_REG, value);
    } else {
        status = SWD_SW_ERROR;
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_raw_read_transaction(uint8_t raw_cmd, uint32_t *value)
{
    swdio_resp_enum_t status = SWD_ACK_OK;
    if (value) {
        swdio_raw_cmd_t cmd;
        cmd.d8 = raw_cmd;
        if (cmd.b.APnDP) {
            status = swdio_ap_read((cmd.b.A3<<3) | (cmd.b.A2<<2), value);
        } else {
            status = swdio_dp_read((cmd.b.A3<<3) | (cmd.b.A2<<2), value);
        }
    } else {
        status = SWD_SW_ERROR;
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_raw_write_transaction(uint8_t raw_cmd, uint32_t value)
{
    swdio_resp_enum_t status;
    swdio_raw_cmd_t cmd;
    cmd.d8 = raw_cmd;
    if (cmd.b.APnDP) {
        status = swdio_ap_write((cmd.b.A3<<3) | (cmd.b.A2<<2), value);
    } else {
        status = swdio_dp_write((cmd.b.A3<<3) | (cmd.b.A2<<2), value);
    }
    return status;
}


void swdio_t::swdio_shift_reset(void)
{
    uint8_t i;
    bool bits[1];
    bits[0] = SWD_BIT_SET;
    swdio_write_bits(bits, 1);
    for (i=0; i<SWDIO_RESET_CLKS; i++) {
        swdio_clk_toggle();
    }
}

void swdio_t::swdio_idle_clocks(void)
{
    uint8_t i;
    swdio_bit_t bits[1];
    bits[0] = SWD_BIT_RESET;
    swdio_write_bits(bits, 1);
    for (i=0; i<SWDIO_IDLE_CLKS; i++) {
        swdio_clk_toggle();
    }
}

void swdio_t::swdio_line_reset(void)
{
    swdio_shift_reset();
    swdio_idle_clocks();
}

void swdio_t::swdio_write_cmd(bool APnDP, bool RnW, uint8_t addr)
{
    swdio_bit_t cmd_value[8];  /* No need to reset, since all bits are populated */
    cmd_value[0] = SWD_BIT_SET;
    cmd_value[1] = APnDP ? SWD_BIT_SET : SWD_BIT_RESET;
    cmd_value[2] = RnW ? SWD_BIT_SET : SWD_BIT_RESET;
    cmd_value[3] = BITOPS_GET_BIT(addr, 2) ? SWD_BIT_SET:SWD_BIT_RESET;
    cmd_value[4] = BITOPS_GET_BIT(addr, 3) ? SWD_BIT_SET:SWD_BIT_RESET;
    cmd_value[5] = swdio_get_parity_bit(&cmd_value[1], 4);

    cmd_value[0] = SWD_BIT_SET;    /* Start bit */
    cmd_value[6] = SWD_BIT_RESET;  /* Stop bit */
    cmd_value[7] = SWD_BIT_SET;    /* Park bit */

    swdio_idle_clocks();
    swdio_write_bits(cmd_value, 8);
}

void swdio_t::swdio_write_data(uint32_t data)
{
    swdio_bit_t data_value[33]; /* No need to reset, since all bits are populated */
    uint8_t i = 0;
    for (i=0; i<32; i++) {
        data_value[i] = (data & (1<<i)) ? SWD_BIT_SET:SWD_BIT_RESET;
    }
    data_value[32] = swdio_get_parity_bit(data_value, 32);
    swdio_write_bits(data_value, 33);
}

swdio_parity_result_t swdio_t::swdio_read_data(uint32_t * data)
{
    swdio_bit_t data_value[33]; /* No need to reset, since all bits are populated */
    swdio_parity_result_t status = SWD_PARITY_CHECK_FAILED;
    swdio_bit_t parity_bit;
    if (data) {
        swdio_read_bits(data_value, 33);
        parity_bit = swdio_get_parity_bit(data_value, 32);
        status = (data_value[32] == parity_bit) ? SWD_PARITY_CHECK_PASSED: SWD_PARITY_CHECK_FAILED;
        if (status==SWD_PARITY_CHECK_PASSED) {
            *data = 0x00;
            for (uint8_t idx=0; idx<32; idx++) {
                *data |= data_value[idx]==SWD_BIT_SET ? 1<<idx : 0;
            }
        }
    } else {
        status = SWD_PARITY_CHECK_FAILED;
    }
    return status;
}

swdio_resp_enum_t swdio_t::swdio_read_resp(void)
{
    swdio_bit_t data_value[3]; /* No need to reset, since all bits are populated */
    swdio_resp_enum_t resp;
    uint8_t value;
    swdio_read_bits(data_value, 3);
    value = data_value[0] | (data_value[1]<<1) | (data_value[2]<<2);
    switch (value) {
        case SWD_ACK_OK:
        case SWD_ACK_WAIT:
        case SWD_ACK_FAULT:
            resp = (swdio_resp_enum_t)value;
            break;
        default:
            resp = SWD_SIGNAL_ERROR;
            break;
    }
    last_resp = resp;
    return resp;
}

swdio_bit_t swdio_t::swdio_get_parity_bit(swdio_bit_t * bits, uint8_t num_bits)
{
    /* Calculate even parity bit using this logic */
    uint8_t num_ones=0;
    uint8_t i;
    for (i=0; i<num_bits; i++) {
        if (bits[i]==SWD_BIT_SET) {
            num_ones++;
        }
    }
    if ((num_ones%2)==1) {
        /* Number of ones is odd */
        return SWD_BIT_SET;
    } else {
        return SWD_BIT_RESET;
    }
}

void swdio_t::swdio_trn_gap()
{
    uint8_t i;
    for (i=0; i<num_trn_clks; i++) {
        swdio_clk_toggle();
    }
}

void swdio_t::swdio_clk_toggle()
{
    DF_DEBUG_STORE();
    GPIO_WriteBit(SWDIO_GPIO_PORT, SWDIO_GPIO_CLK_PIN, Bit_SET);
    swdio_delay_us(1);
    DF_DEBUG_STORE();
    GPIO_WriteBit(SWDIO_GPIO_PORT, SWDIO_GPIO_CLK_PIN, Bit_RESET);
    swdio_delay_us(1);
}

void swdio_t::swdio_write_bits(swdio_bit_t * bits, uint8_t num_bits)
{
    uint8_t i;
    swdio_set_io_as_output();
    for (i=0; i<num_bits; i++) {
        GPIO_WriteBit(SWDIO_GPIO_PORT, SWDIO_GPIO_IO_PIN, bits[i]==SWD_BIT_SET ? Bit_SET:Bit_RESET);
        swdio_clk_toggle();
    }
}

void swdio_t::swdio_read_bits(swdio_bit_t * bits, uint8_t num_bits)
{
    uint8_t i;
    swdio_set_io_as_input();
    for (i=0; i<num_bits; i++) {
        bits[i] = 
            GPIO_ReadInputDataBit(SWDIO_GPIO_PORT, SWDIO_GPIO_IO_PIN)==Bit_SET ? SWD_BIT_SET:SWD_BIT_RESET;
        swdio_clk_toggle();
    }
}

void swdio_process_command(swdio_cmd_t *cmd, swdio_resp_t *resp)
{
    if (!g_swdio_handle) {
        g_swdio_handle = swdio_t::get_instance();
    }
    swdio_resp_enum_t core_resp = SWD_SW_ERROR;
    uint32_t value = 0x00;
    if (cmd && resp) {
        memset(resp, 0x00, sizeof(*resp));
        resp->cmd = cmd->cmd;
        switch (cmd->cmd) {
            case SWDIO_CMD_DP_CONNECT:
                {
                    core_resp = g_swdio_handle->swdio_dp_connect(&value);
                    resp->data.id.idx = 0;
                    resp->data.id.id_code = value;
                }
                break;
            case SWDIO_CMD_DP_GET_IDCODE:
                {
                    core_resp = g_swdio_handle->swdio_dp_get_idcode(&value);
                    resp->data.id.idx = 0;
                    resp->data.id.id_code = value;
                }
                break;
            case SWDIO_CMD_AP_SELECT:
                {
                    core_resp = g_swdio_handle->swdio_ap_select(cmd->data.select.ap_sel, 
                        cmd->data.select.bank_sel, cmd->data.select.ctrl_sel);
                }
                break;
            case SWDIO_CMD_AP_GET_IDCODE:
                {
                    core_resp = g_swdio_handle->swdio_ap_get_idcode(cmd->data.id.idx, &value);
                    resp->data.id.idx = cmd->data.id.idx;
                    resp->data.id.id_code = value;
                }
                break;
            case SWDIO_CMD_DP_READ:
                {
                    core_resp = g_swdio_handle->swdio_dp_read(cmd->data.rw.reg, &value);
                    resp->data.rw.reg = cmd->data.rw.reg;
                    resp->data.rw.value = value;
                }
                break;
            case SWDIO_CMD_DP_WRITE:
                {
                    core_resp = g_swdio_handle->swdio_dp_write(cmd->data.rw.reg, cmd->data.rw.value);
                    resp->data.rw.reg = cmd->data.rw.reg;
                    resp->data.rw.value = cmd->data.rw.value;
                }
                break;
            case SWDIO_CMD_AP_READ:
                {
                    core_resp = g_swdio_handle->swdio_ap_read(cmd->data.rw.reg, &value);
                    resp->data.rw.reg = cmd->data.rw.reg;
                    resp->data.rw.value = value;
                }
                break;
            case SWDIO_CMD_AP_WRITE:
                {
                    core_resp = g_swdio_handle->swdio_ap_write(cmd->data.rw.reg, cmd->data.rw.value);
                    resp->data.rw.reg = cmd->data.rw.reg;
                    resp->data.rw.value = cmd->data.rw.value;
                }
                break;

            case SWDIO_CMD_RAW_READ_TRANSACTION:
                {
                    core_resp = g_swdio_handle->swdio_raw_read_transaction(cmd->data.raw.raw_cmd, &value);
                    resp->data.raw.raw_cmd = cmd->data.raw.raw_cmd;
                    resp->data.raw.raw_value = value;
                }
                break;
            case SWDIO_CMD_RAW_WRITE_TRANSACTION:
                {
                    core_resp = g_swdio_handle->swdio_raw_write_transaction(cmd->data.raw.raw_cmd, cmd->data.raw.raw_value);
                    resp->data.raw.raw_cmd = cmd->data.raw.raw_cmd;
                    resp->data.raw.raw_value = cmd->data.raw.raw_value;
                }
                break;
            default:
                df_assert(0);
        }

        switch (core_resp) {
            case SWD_ACK_OK:
                resp->status = SWDIO_RESP_STATUS_OK;
                break;
            case SWD_SIGNAL_ERROR:
                resp->status = SWDIO_RESP_STATUS_SIGNAL_ERROR;
                break;
            case SWD_SW_ERROR:
                df_assert(0);
                resp->status = SWDIO_RESP_STATUS_SW_ERROR;
                break;
            case SWD_ACK_WAIT:
                resp->status = SWDIO_RESP_STATUS_WAIT;
                break;
            case SWD_ACK_FAULT:
                resp->status = SWDIO_RESP_STATUS_FAULT;
                break;
            case SWD_PARITY_ERROR:
                resp->status = SWDIO_RESP_STATUS_PARITY_ERROR;
                break;
            default:
                df_assert(0);
        }
    
        if (core_resp==SWD_ACK_FAULT) {
            /* Since we know the error, clear the sticky bits */
        }
    } else {
        df_assert(0);
    }
}


