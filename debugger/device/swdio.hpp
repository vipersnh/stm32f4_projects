#ifndef __SWDIO_HPP_
#define __SWDIO_HPP_

#define SWDIO_RESET_CLKS    64
#define SWDIO_IDLE_CLKS      8



typedef enum {
    SWD_ACK_OK      = 0b0001,
    SWD_ACK_WAIT    = 0b0010,
    SWD_ACK_FAULT   = 0b0100,
    SWD_PARITY_ERROR= 0b1000,
    SWD_SIGNAL_ERROR,
    SWD_SW_ERROR,
} swdio_resp_enum_t;

typedef enum {
    SWD_PARITY_CHECK_PASSED,
    SWD_PARITY_CHECK_FAILED,
} swdio_parity_result_t;

typedef bool swdio_bit_t;

typedef enum {
    SWD_BIT_RESET   = 0x00,
    SWD_BIT_SET     = 0x01,
} swdio_bit_enum_t;

#define SWDIO_JTAG_TO_SWD_SEQ 0xE79E
#define SWDIO_SWD_TO_JTAG_SEQ 0xE73C

#define SWDIO_DEFAULT_TRN_CLKS 1

#define SWDIO_DP_REG_VALIDATE(_reg) \
    (((_reg)<=0x0C) && (((_reg)&0x03)==0))


#define SWDIO_AP_REG_VALIDATE(reg)  true

#define SWDIO_GPIO_PORT     GPIOD

#define SWDIO_GPIO_RST_PIN  GPIO_Pin_0
#define SWDIO_GPIO_IO_PIN   GPIO_Pin_1
#define SWDIO_GPIO_CLK_PIN  GPIO_Pin_2

class swdio_t {
    public:

        /* Class instantiation operations */
        swdio_t(void);
        static swdio_t * get_instance();
        void swdio_gpio_init();

        /* SWD high level abstraction operations */
        swdio_resp_enum_t swdio_dp_connect(uint32_t * idcode);
        swdio_resp_enum_t swdio_dp_get_idcode(uint32_t * idcode);
        swdio_resp_enum_t swdio_ap_select(uint8_t ap_sel, uint8_t bank_sel, bool ctrlSel=0);
        swdio_resp_enum_t swdio_ap_get_idcode(uint8_t ap_idx, uint32_t * idcode);

        /* SWD high level core operations */
        swdio_resp_enum_t swdio_dp_read ( uint8_t reg, uint32_t  * value);   /* Populate value read from DP-register specified */ 
        swdio_resp_enum_t swdio_dp_write( uint8_t reg, uint32_t    value);   /* Write the value specified into the DP-register specified */ 
        swdio_resp_enum_t swdio_ap_read (uint32_t reg, uint32_t  * value);   /* Populate value read from AP-register specified */  
        swdio_resp_enum_t swdio_ap_write(uint32_t reg, uint32_t    value);   /* Write the value specified into the DP-register specified */ 
        swdio_resp_enum_t swdio_dp_read_status(uint32_t *value);             /* Read the DP-CTRL/STATUS register from DP */

        /* SWD high level raw operations */
        swdio_resp_enum_t swdio_raw_read_transaction(uint8_t raw_cmd, uint32_t *value);
        swdio_resp_enum_t swdio_raw_write_transaction(uint8_t raw_cmd, uint32_t value);
    private:
        /* SWD physical level protocol operations */
        void swdio_shift_reset(void);               /* Performs swdio line reset */
        void swdio_idle_clocks(void);               /* Insert idle clocks on SWD interface */
        void swdio_line_reset(void);               /* Performs swdio line reset */
        void swdio_write_cmd(bool APnDP, bool RnW, uint8_t addr);
        void swdio_write_data(uint32_t data);
        swdio_parity_result_t swdio_read_data(uint32_t * data);
        swdio_resp_enum_t swdio_read_resp(void);

        /* SWD physical level bit operations */
        swdio_resp_enum_t last_resp;
        uint8_t num_trn_clks;
        swdio_bit_t swdio_get_parity_bit(swdio_bit_t * bits, uint8_t num_bits);    /* Returns true or false following EVEN parity for all bits including parity bit */
        void swdio_set_io_as_output(void);
        void swdio_set_io_as_input(void);
        void swdio_set_clk_as_output(void);
        void swdio_set_clk_as_input(void);
        void swdio_trn_gap();
        void swdio_clk_toggle();
        void swdio_write_bits(swdio_bit_t * bits, uint8_t num_bits);
        void swdio_read_bits(swdio_bit_t * bits, uint8_t num_bits);

    protected:
        static swdio_t    * instance;
        swdio_dp_status_t   control_register;

};

extern void swdio_process_command(swdio_cmd_t *cmd, swdio_resp_t *resp);

#endif
