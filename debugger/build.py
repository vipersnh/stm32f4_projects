#!python
import sys
sys.path.insert(0, '../common')

import make
import build_common

profile_main = make.profile_t()

include_dirs = ["./", "./common", "./device", "../../drivers/usb_device/device"] + build_common.profile.include_dirs
source_dirs = build_common.profile.source_dirs
source_files = build_common.profile.source_files + ["./main.cpp", "./syscalls.c", "system_stm32f4xx.c",
        "./stm32f4xx_it.c",
        "./device/swdio.cpp", "usb_device.cpp", "stm32f4xx_usb.cpp",
        "../../drivers/usb_device/device/tests/usb_comm_intf_device_ut.cpp",
        "../../drivers/usb_device/device/usb_comm_intf_device.cpp",
        "../../drivers/usb_device/device/usb_core_device.cpp",
        "../../drivers/usb_device/device/usb_core_common.cpp"]
default_c_cpp_flags = ["-DSTM32F4XX_USB_DRIVER=1", "-enum_is_int"]
cflags = build_common.profile.cflags + default_c_cpp_flags
cxxflags = build_common.profile.cxxflags + default_c_cpp_flags
ldflags =  ["--static"] + build_common.profile.ldflags
linker_scripts = build_common.profile.linker_scripts
libraries = ["-lm", "-lc"]
arflags = list()

profile_main.name = "main"
profile_main.cc = "arm-none-eabi-gcc"
profile_main.cxx = "arm-none-eabi-g++"
profile_main.ld  = "arm-none-eabi-g++"
profile_main.objcopy = "arm-none-eabi-objcopy"
profile_main.objdump = "arm-none-eabi-objdump"
profile_main.include_dirs = include_dirs
profile_main.source_dirs = source_dirs
profile_main.source_files = source_files
profile_main.cflags = cflags
profile_main.cxxflags = cxxflags
profile_main.ldflags = ldflags
profile_main.linker_scripts = linker_scripts
profile_main.libraries = libraries
profile_main.arflags = arflags
profile_main.executable = "main.elf"
profile_main.library = None
profile_main.verbose_level = make.verbosity.MINIMAL
profile_main.build_dir = "./build"
profile_main.executable_subtargets["iHex"] = [profile_main.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.hex"]
profile_main.executable_subtargets["binary"] = [profile_main.objcopy, "-O", "binary", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.bin"]
profile_main.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_main) + "main.bin",
    "0x8000000"]

profile_host_ut = make.profile_t()
profile_host_ut.name = "ut_swdio_host"
profile_host_ut.include_dirs = [".", "./common", "./host", "./host/tests", "../../drivers/usb_device/device", "../../drivers/usb_device/host",
    "../../cpp_library/df", "../../cpp_library/cbuf"]
profile_host_ut.cc = "gcc"
profile_host_ut.cxx = "g++"
profile_host_ut.ld  = "g++"
profile_host_ut.cflags = ["-g", "-Wall", "-enum_is_int"]
profile_host_ut.cxxflags = ["-g", "-Wall", "-enum_is_int"]
profile_host_ut.ldflags = ["-g", "-Wall"]
profile_host_ut.libraries = ["-lusb-1.0"]
profile_host_ut.executable = "swdio_host.out"
profile_host_ut.build_dir = "./build"
profile_host_ut.source_dirs = ["./host", "./host/tests", "./../../drivers/usb_device/host"]
profile_host_ut.source_files = ["../../cpp_library/df/df_base.cpp"]

profile_host_pyshared_lib = make.profile_t()
profile_host_pyshared_lib.name = "py_swdio_host"
profile_host_pyshared_lib.include_dirs = [".", "./common", "./host", "./host/tests", "../../drivers/usb_device/device", "../../drivers/usb_device/host",
    "../../cpp_library/df", "../../cpp_library/cbuf", "/usr/local/include/python3.4m"]
profile_host_pyshared_lib.cc = "gcc"
profile_host_pyshared_lib.cxx = "g++"
profile_host_pyshared_lib.ld  = "g++"
profile_host_pyshared_lib.cflags = ["-g", "-Wall", "-enum_is_int", "-fPIC"]
profile_host_pyshared_lib.cxxflags = ["-g", "-Wall", "-enum_is_int", "-fPIC"]
profile_host_pyshared_lib.ldflags = ["-shared"]
profile_host_pyshared_lib.libraries = ["-lusb-1.0", "-lpython3.4m","-lc", "-lpthread", ]
profile_host_pyshared_lib.build_dir = "./build"
profile_host_pyshared_lib.pre_commands["swig"] = ["swig", "-python", "-c++", "-o", "./build/py_swdio_host/swdio_host_intf_swig.cpp", "./host/swdio_host_intf_swig.i"]
profile_host_pyshared_lib.executable = "_swdio_host_intf_python.so"
profile_host_pyshared_lib.source_dirs = ["./host", "./../../drivers/usb_device/host"]
profile_host_pyshared_lib.source_files = ["../../cpp_library/df/df_base.cpp", "./build/py_swdio_host/swdio_host_intf_swig.cpp"]

profile_host_shared_lib = make.profile_t()
profile_host_shared_lib.name = "swdio_host_shared_lib"
profile_host_shared_lib.include_dirs = [".", "./common", "./host", "./host/tests", "../../drivers/usb_device/device", "../../drivers/usb_device/host",
    "../../cpp_library/df", "../../cpp_library/cbuf"]
profile_host_shared_lib.cc = "gcc"
profile_host_shared_lib.cxx = "g++"
profile_host_shared_lib.ld  = "g++"
profile_host_shared_lib.cflags = ["-g", "-Wall", "-enum_is_int", "-fPIC"]
profile_host_shared_lib.cxxflags = ["-g", "-Wall", "-enum_is_int", "-fPIC"]
profile_host_shared_lib.ldflags = ["-shared"]
profile_host_shared_lib.libraries = ["-lpthread"]
profile_host_shared_lib.build_dir = "./build"
profile_host_shared_lib.executable = "libswdio.so"
profile_host_shared_lib.source_dirs = ["./host", "./../../drivers/usb_device/host"]
profile_host_shared_lib.source_files = ["../../cpp_library/df/df_base.cpp"]


profiles = [profile_main, profile_host_ut, profile_host_pyshared_lib, profile_host_shared_lib]

make.build(profiles_list = profiles, args = sys.argv[1:])
