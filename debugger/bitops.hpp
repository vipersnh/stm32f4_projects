#ifndef __BITOPS_HPP_

#define BITOPS_SHIFTED_VALUE(_value, _shift, _num_bits) \
    (((_value) & ((1<<(_num_bits))-1)) << (_shift))

#define BITOPS_GET_BIT(_value, _bit_num) \
    ((_value&(1<<_bit_num))>>_bit_num)

#endif
