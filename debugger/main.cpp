#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "FreeRTOS.h"
    #include "task.h"
}

#include "df_base.h"
#include "cbuf_packet.h"
#include "cbuf_char.h"
#include "usb_device.h"
#include "stm32f4xx_usb.hpp"
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_device.hpp"

#include "swdio_intf.hpp"
#include "swdio.hpp"

void assert_led(void)
{
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
}


void task_1(void *p)
{
    while(1) {
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        vTaskDelay(1000);
        GPIO_ResetBits(GPIOD, GPIO_Pin_13);
        vTaskDelay(1000);
    }
}

void swdio_command_task(void *p)
{
    usb_comm_t * handle = usb_comm_t::get_instance();
    usb_comm_packet_t   recv_packet = {0};
    usb_comm_packet_t   send_packet = {0};
    swdio_cmd_t *cmd;
    swdio_resp_t *resp;
    send_packet.hdr.pkt_type = USB_COMM_DATA;
    send_packet.hdr.ch_id = SWDIO_USB_CH_ID;
    send_packet.hdr.length = sizeof(usb_comm_intf_hdr_t) + sizeof(swdio_resp_t);
    while (1) {
        if (handle->usb_comm_get_num_packets_received()) {
            handle->usb_comm_recv_deque_packet(&recv_packet);
            df_assert(recv_packet.hdr.ch_id==SWDIO_USB_CH_ID);
            cmd = (swdio_cmd_t*)recv_packet.data;
            resp = (swdio_resp_t*)send_packet.data;
            swdio_process_command(cmd, resp);
            handle->usb_comm_send_enque_packet(&send_packet);
        } else {
            vTaskDelay(1);
        }
    }
}

void init(void) {
    SystemInit();
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13| GPIO_Pin_14| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
    usb_device_initialize();
}


int main() {
	init();
    xTaskCreate(task_1, (char *)"task_1", 500,0 ,tskIDLE_PRIORITY+1, 0);
    xTaskCreate(swdio_command_task, (char *)"swdio_command_task", 500,0 ,tskIDLE_PRIORITY+2, 0);
    vTaskStartScheduler();
    while (1) { };
	return -1;
}
