extern "C" {
    #include <stdint.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
}

#include "df_base.h"
#include "swdio_intf.hpp"
#include "swdio_host_intf.hpp"
#include "swdio_host_intf_ut.hpp"

typedef struct {
    bool    is_write;
    uint8_t raw_cmd;
    uint32_t value;
} raw_swd_seq_t;

raw_swd_seq_t sequence[] = {
    {false,  0xc , },      
    {true ,  0x28, 0x20},
    {false,  0xc , },        
    {true ,  0x28, 0x50000000},
    {false,  0xc , }, 
    {false,  0xc , }, 
    {false,  0xc , }, 
    {true ,  0x28, 0x50000001},
    {false,  0xc , }, 
    {true ,  0x30, 0x0},
    {true ,  0x22, 0xa2000020},
    {true ,  0xa , 0x0},
    {false,  0x6 , }, 
    {false,  0x3c, }, 
    {true ,  0x30, 0xf0},
    {false,  0x2e, }, 
    {false,  0x3c, }, 
    {false,  0x1e, }, 
    {false,  0x3c, },
    {true ,  0x30, 0x10000f0},
    {false,  0x1e, },
    {false,  0x3c, },
    {true ,  0x30, 0xf0},
    {false,  0x1e, },
    {false,  0x3c, },
    {true ,  0x30, 0x1000000},
    {true ,  0x22, 0xa2000002},
    {true ,  0xa , 0x1ffe0d00},
    {true ,  0x30, 0x1000010},
    {false,  0x6 , }, 
    {false,  0x3c, }, 
    {false,  0x6 , }, 
    {false,  0x3c, }, 
    {false,  0x2e, }, 
    {false,  0x3c, }, 
    {false,  0x1e, }, 
    {false,  0x3c, },      
};

int main()
{
    swdio_resp_status_enum_t status;
    if (!swdio_init()) {
        printf("SWDIO Initialization failed\n");
        return 1;
    }

    uint32_t idcode;
    status = swdio_dp_connect(&idcode);
    printf("Status = %d, id = 0x%x\n", status, idcode);

    for (uint32_t seq_idx = 0; seq_idx < df_arraylen(sequence); seq_idx++) {
        raw_swd_seq_t * sq = sequence + seq_idx;
        if (sq->is_write) {
            status = swdio_raw_write_transaction(sq->raw_cmd, sq->value);
        } else {
            status = swdio_raw_read_transaction(sq->raw_cmd, &sq->value);
        }
        swdio_raw_cmd_t cmd;
        cmd.d8 = sq->raw_cmd;
        printf("Executing swd sequence[%03d] : [%s : %s], reg[0x%02x], value[0x%08x], status[%d]\n", seq_idx,
            cmd.b.APnDP ? "AP" : "DP", 
            sq->is_write ? "WR" : "RD", (sq->raw_cmd & 0x18)>>1, sq->value, status);
    }
    return 0;
}
