%module swdio_host_intf_python
%include "cpointer.i"
%include "carrays.i"
%{
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "df_base.h"
#include "cbuf_packet.h"
#include <libusb-1.0/libusb.h>
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_host.hpp"

#include "swdio_intf.hpp"
#include "swdio_host_intf.hpp"
%}
%include "swdio_host_intf.hpp"
%include "stdint.i"
%pointer_functions(uint32_t, uint32_t_ptr)
