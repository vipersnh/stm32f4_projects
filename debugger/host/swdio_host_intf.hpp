#ifndef _SWDIO_HOST_INTF_HPP_
#define _SWDIO_HOST_INTF_HPP_

#ifdef __cplusplus
extern "C" {
#endif

extern bool swdio_init(void);
extern swdio_resp_status_enum_t swdio_dp_connect(uint32_t * idcode);
extern swdio_resp_status_enum_t swdio_dp_get_idcode(uint32_t * idcode);
extern swdio_resp_status_enum_t swdio_ap_select(uint8_t ap_sel, uint8_t bank_sel, bool ctrl_sel);
extern swdio_resp_status_enum_t swdio_ap_get_idcode(uint8_t ap_idx, uint32_t * idcode);

extern swdio_resp_status_enum_t swdio_dp_read ( uint8_t reg, uint32_t  * value);
extern swdio_resp_status_enum_t swdio_dp_write( uint8_t reg, uint32_t    value);
extern swdio_resp_status_enum_t swdio_ap_read (uint32_t reg, uint32_t  * value);
extern swdio_resp_status_enum_t swdio_ap_write(uint32_t reg, uint32_t    value);
extern swdio_resp_status_enum_t swdio_dp_read_ctrl_status(uint32_t *value);
extern swdio_resp_status_enum_t swdio_dp_write_ctrl_status(uint32_t value);
extern swdio_resp_status_enum_t swdio_dp_get_sticky_bits(bool *ovr_run, bool *wrt_error, 
    bool *sticky_error, bool *sticky_compare);
extern swdio_resp_status_enum_t swdio_dp_clear_sticky_bits(bool ovr_run, bool wrt_error,
    bool sticky_error, bool sticky_compare);
extern swdio_resp_status_enum_t swdio_dp_set_abort(void);

extern swdio_resp_status_enum_t swdio_raw_read_transaction(uint8_t cmd, uint32_t *value);
extern swdio_resp_status_enum_t swdio_raw_write_transaction(uint8_t cmd, uint32_t value);

#ifdef __cplusplus
}
#endif


#endif
