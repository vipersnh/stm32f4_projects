extern "C" {
    #include <stdint.h>
    #include <stdio.h>
    #include <string.h>
    #include <stdlib.h>
    #include <unistd.h>
}
#include "df_base.h"
#include "cbuf_packet.h"
#include <libusb-1.0/libusb.h>
#include "usb_comm_intf.hpp"
#include "usb_comm_intf_host.hpp"

#include "swdio_intf.hpp"
#include "swdio_host_intf.hpp"

static bool initialized = false;
static usb_comm_t * handle = NULL;

bool swdio_init()
{
    if (handle==NULL) {
        handle = usb_comm_t::get_instance();
        initialized = handle->usb_comm_init(0xF0F0, 0xA0B0);
    }
    return initialized;
}

swdio_resp_status_enum_t swdio_dp_connect(uint32_t * idcode)
{
    if (idcode) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_DP_CONNECT;
        cmd.data.id.idx = 0;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        if (resp.status==SWDIO_RESP_STATUS_OK && idcode) {
            *idcode = resp.data.id.id_code;
        } else {
            *idcode = 0x00;
        }
        return resp.status;
    } else {
        return SWDIO_RESP_STATUS_SW_ERROR;
    }
}

swdio_resp_status_enum_t swdio_dp_get_idcode(uint32_t * idcode)
{
    if (idcode) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_DP_GET_IDCODE;
        cmd.data.id.idx = 0;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        if (resp.status==SWDIO_RESP_STATUS_OK) {
            *idcode = resp.data.id.id_code;
        } else {
            *idcode = 0x00;
        }
        return resp.status;
    }
    return SWDIO_RESP_STATUS_SW_ERROR;
}

swdio_resp_status_enum_t swdio_ap_select(uint8_t ap_sel, uint8_t bank_sel, bool ctrl_sel)
{
    bool status;
    uint8_t ch_id = 0;
    swdio_cmd_t cmd;
    swdio_resp_t resp;
    memset(&cmd, 0x00, sizeof(cmd));
    memset(&resp, 0x00, sizeof(resp));
    cmd.cmd = SWDIO_CMD_AP_SELECT;
    cmd.data.select.ap_sel = ap_sel;
    cmd.data.select.bank_sel = bank_sel;
    cmd.data.select.ctrl_sel = ctrl_sel;
    status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
    df_assert(status);
    status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
    df_assert(status);
    df_assert(resp.cmd==cmd.cmd);
    return resp.status;
}

swdio_resp_status_enum_t swdio_ap_get_idcode(uint8_t ap_idx, uint32_t * idcode)
{
    if (idcode) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_AP_GET_IDCODE;
        cmd.data.id.idx = ap_idx;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        df_assert(resp.data.id.idx==cmd.data.id.idx);
        if (resp.status==SWDIO_RESP_STATUS_OK) {
            *idcode = resp.data.id.id_code;
        } else {
            *idcode = 0x00;
        }
        return resp.status;
    }
    return SWDIO_RESP_STATUS_SW_ERROR;
}

swdio_resp_status_enum_t swdio_dp_read ( uint8_t reg, uint32_t  * value)
{
    if (value) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_DP_READ;
        cmd.data.rw.reg = reg;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        df_assert(resp.data.rw.reg==cmd.data.rw.reg);
        if (resp.status==SWDIO_RESP_STATUS_OK) {
            *value = resp.data.rw.value;
        } else {
            *value = 0x00;
        }
        return resp.status;
    }
    return SWDIO_RESP_STATUS_SW_ERROR;
}

swdio_resp_status_enum_t swdio_dp_write( uint8_t reg, uint32_t    value)
{
    bool status;
    uint8_t ch_id = 0;
    swdio_cmd_t cmd;
    swdio_resp_t resp;
    memset(&cmd, 0x00, sizeof(cmd));
    memset(&resp, 0x00, sizeof(resp));
    cmd.cmd = SWDIO_CMD_DP_WRITE;
    cmd.data.rw.reg = reg;
    cmd.data.rw.value = value;
    status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
    df_assert(status);
    status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
    df_assert(status);
    df_assert(resp.cmd==cmd.cmd);
    df_assert(resp.data.rw.reg==cmd.data.rw.reg);
    df_assert(resp.data.rw.value==cmd.data.rw.value);
    return resp.status;
}

swdio_resp_status_enum_t swdio_ap_read (uint32_t reg, uint32_t  * value)
{
    if (value) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_AP_READ;
        cmd.data.rw.reg = reg;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        df_assert(resp.data.rw.reg==cmd.data.rw.reg);
        if (resp.status==SWDIO_RESP_STATUS_OK) {
            *value = resp.data.rw.value;
        } else {
            *value = 0x00;
        }
        return resp.status;
    }
    return SWDIO_RESP_STATUS_SW_ERROR;
}
swdio_resp_status_enum_t swdio_ap_write(uint32_t reg, uint32_t    value)
{
    bool status;
    uint8_t ch_id = 0;
    swdio_cmd_t cmd;
    swdio_resp_t resp;
    memset(&cmd, 0x00, sizeof(cmd));
    memset(&resp, 0x00, sizeof(resp));
    cmd.cmd = SWDIO_CMD_AP_WRITE;
    cmd.data.rw.reg = reg;
    cmd.data.rw.value = value;
    status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
    df_assert(status);
    status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
    df_assert(status);
    df_assert(resp.cmd==cmd.cmd);
    df_assert(resp.data.rw.reg==cmd.data.rw.reg);
    df_assert(resp.data.rw.value==cmd.data.rw.value);
    return resp.status;
}


swdio_resp_status_enum_t swdio_dp_read_ctrl_status(uint32_t *value)
{
    return swdio_dp_read(SWD_DP_CTRL_STAT_RW_WCR_RW_REG, value);
}

swdio_resp_status_enum_t swdio_dp_write_ctrl_status(uint32_t value)
{
    return swdio_dp_write(SWD_DP_CTRL_STAT_RW_WCR_RW_REG, value);
}

swdio_resp_status_enum_t swdio_dp_get_sticky_bits(bool *ovr_run, bool *wrt_error, 
    bool *sticky_error, bool *sticky_compare)
{
    swdio_dp_status_t dp_status = {0};
    swdio_resp_status_enum_t status = swdio_dp_read_ctrl_status(&dp_status.d32);
    if (status==SWDIO_RESP_STATUS_OK) {
        if (ovr_run) {
            *ovr_run = dp_status.b.bit_STICKYORUN; 
        }
        if (wrt_error) {
            *wrt_error = dp_status.b.bit_WDATAERR;
        }
        if (sticky_error) {
            *sticky_error = dp_status.b.bit_STICKYERR;
        }
        if (sticky_compare) {
            *sticky_compare = dp_status.b.bit_STICKYCMP;
        }
    }
    return status;
}

swdio_resp_status_enum_t swdio_dp_clear_sticky_bits(bool ovr_run, bool wrt_error, bool sticky_error, 
    bool sticky_compare)
{
    swdio_dp_abort_t dp_abort = {0};
    dp_abort.b.bit_ORUNERRCLR = ovr_run;
    dp_abort.b.bit_WDERRCLR = wrt_error;
    dp_abort.b.bit_STKERRCLR = sticky_error;
    dp_abort.b.bit_STKCMPCLR = sticky_compare;
    return swdio_dp_write(SWD_DP_IDCODE_R_ABORT_W_REG, dp_abort.d32);
}


swdio_resp_status_enum_t swdio_dp_set_abort(void)
{
    swdio_dp_abort_t dp_abort = {0};
    dp_abort.b.bit_DAPABORT = 1;
    return swdio_dp_write(SWD_DP_IDCODE_R_ABORT_W_REG, dp_abort.d32);
}

swdio_resp_status_enum_t swdio_raw_read_transaction(uint8_t raw_cmd, uint32_t *value)
{
    if (value) {
        bool status;
        uint8_t ch_id = 0;
        swdio_cmd_t cmd;
        swdio_resp_t resp;
        memset(&cmd, 0x00, sizeof(cmd));
        memset(&resp, 0x00, sizeof(resp));
        cmd.cmd = SWDIO_CMD_RAW_READ_TRANSACTION;
        cmd.data.raw.raw_cmd = raw_cmd;
        status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
        df_assert(status);
        status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
        df_assert(status);
        df_assert(resp.cmd==cmd.cmd);
        df_assert(resp.data.raw.raw_cmd==cmd.data.raw.raw_cmd);
        if (resp.status==SWDIO_RESP_STATUS_OK) {
            *value = resp.data.raw.raw_value;
        } else {
            *value = 0x00;
        }
        return resp.status;
    }
    return SWDIO_RESP_STATUS_SW_ERROR;
}

swdio_resp_status_enum_t swdio_raw_write_transaction(uint8_t raw_cmd, uint32_t value)
{
    bool status;
    uint8_t ch_id = 0;
    swdio_cmd_t cmd;
    swdio_resp_t resp;
    memset(&cmd, 0x00, sizeof(cmd));
    memset(&resp, 0x00, sizeof(resp));
    cmd.cmd = SWDIO_CMD_RAW_WRITE_TRANSACTION;
    cmd.data.raw.raw_cmd = raw_cmd;
    cmd.data.raw.raw_value = value;
    status = handle->usb_comm_send_packet(SWDIO_USB_CH_ID, (uint8_t*)&cmd, sizeof(cmd));
    df_assert(status);
    status = handle->usb_comm_receive_packet(&ch_id, (uint8_t*)&resp, sizeof(resp));
    df_assert(status);
    df_assert(resp.cmd==cmd.cmd);
    df_assert(resp.data.raw.raw_cmd==cmd.data.raw.raw_cmd);
    df_assert(resp.data.raw.raw_value==cmd.data.raw.raw_value);
    return resp.status;
}
