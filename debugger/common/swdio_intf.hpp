#ifndef _SWDIO_INTF_HPP_
#define _SWDIO_INTF_HPP_

#define SWDIO_USB_CH_ID          1

#define SWDIO_CMD_MAX_DATA_LEN   3   /* In words */
#define SWDIO_RESP_MAX_DATA_LEN  1   /* In words */

typedef enum {
    SWD_DP_IDCODE_R_ABORT_W_REG     = 0x00,
    SWD_DP_CTRL_STAT_RW_WCR_RW_REG  = 0x04,
    SWD_DP_RD_RESEND_R_APSEL_W_REG  = 0x08,
    SWD_DP_RDBUF_R_REG              = 0x0C,
} swdio_dp_register_addr_enum_t;

typedef enum {
    SWD_AP_IDCODE_REG = 0xFC,
} swdio_ap_register_addr_enum_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t bit_ORUNDETECT : 1;
        uint32_t bit_STICKYORUN : 1;
        uint32_t bit_TRNMODE_2_3: 2;
        uint32_t bit_STICKYCMP : 1; 
        uint32_t bit_STICKYERR : 1; 
        uint32_t bit_READOK : 1; 
        uint32_t bit_WDATAERR : 1; 
        uint32_t bit_MASKLANE_8_11 : 4; 
        uint32_t bit_TRNCNT_12_21 : 10; 
        uint32_t bit_Reserved_24_25 : 2; 
        uint32_t bit_CDBGRSTREQ : 1;
        uint32_t bit_CDBGRSTACK : 1; 
        uint32_t bit_CDBGPWRUPREQ : 1; 
        uint32_t bit_CDBGPWRUPACK : 1; 
        uint32_t bit_CSYSPWRUPREQ : 1; 
        uint32_t bit_CSYSPWRUPACK : 1;
    } b;
} swdio_dp_status_t;

typedef union {
    uint32_t d32;
    struct {
        uint32_t bit_DAPABORT : 1;
        uint32_t bit_STKCMPCLR : 1;
        uint32_t bit_STKERRCLR : 1;
        uint32_t bit_WDERRCLR : 1;
        uint32_t bit_ORUNERRCLR : 1;
        uint32_t bit_Reserved_5_31 : 27;
    } b;
} swdio_dp_abort_t;

typedef union {
    uint8_t d8;
    struct {
        uint8_t start : 1;
        uint8_t APnDP : 1;
        uint8_t RnW   : 1;
        uint8_t A2    : 1;
        uint8_t A3    : 1;
        uint8_t parity: 1;
        uint8_t stop  : 1;
        uint8_t park  : 1;
    } b;
} swdio_raw_cmd_t;

typedef enum {
    SWDIO_CMD_DP_CONNECT,
    SWDIO_CMD_DP_GET_IDCODE,
    SWDIO_CMD_AP_SELECT,
    SWDIO_CMD_AP_GET_IDCODE,

    SWDIO_CMD_DP_READ,
    SWDIO_CMD_DP_WRITE,
    SWDIO_CMD_AP_READ,
    SWDIO_CMD_AP_WRITE,

    SWDIO_CMD_RAW_READ_TRANSACTION,
    SWDIO_CMD_RAW_WRITE_TRANSACTION,
} _PACKED_ swdio_cmd_enum_t;

typedef struct {
    swdio_cmd_enum_t cmd;
    union {
        struct {
            uint32_t ap_sel;
            uint32_t bank_sel;
            uint32_t ctrl_sel;
        } _PACKED_ select;

        struct {
            uint32_t idx;
        } _PACKED_ id;

        struct {
            uint32_t reg;
            uint32_t value;
        } _PACKED_ rw;

        struct {
            uint8_t  raw_cmd;
            uint32_t raw_value;
        } _PACKED_ raw;
    } _PACKED_ data;
} _PACKED_ swdio_cmd_t;

typedef enum {
    SWDIO_RESP_STATUS_OK,
    SWDIO_RESP_STATUS_WAIT,
    SWDIO_RESP_STATUS_FAULT,
    SWDIO_RESP_STATUS_PARITY_ERROR,
    SWDIO_RESP_STATUS_SIGNAL_ERROR,
    SWDIO_RESP_STATUS_SW_ERROR,
} _PACKED_ swdio_resp_status_enum_t;

typedef struct {
    swdio_cmd_enum_t         cmd;
    swdio_resp_status_enum_t status;
    uint8_t                  phy_resp;
    union {
        struct {
            uint32_t idx;
            uint32_t id_code;
        } _PACKED_ id;

        struct {
            uint32_t reg;
            uint32_t value;
        } _PACKED_ rw;
    
        struct {
            uint32_t value;
        } _PACKED_ status;

        struct {
            uint8_t  raw_cmd;
            uint32_t raw_value;
        } _PACKED_ raw;
    } _PACKED_ data;
} _PACKED_ swdio_resp_t;

#endif
