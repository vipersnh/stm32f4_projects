#!python
import sys
from pdb import set_trace
sys.path.append("/hdd1/data/projects/vipersnh_git/python_library/make")
sys.path.insert(0, '../common')

import make
import copy
import build_common

profile_main = make.profile_t()

include_dirs = ["./"] + build_common.profile.include_dirs 
source_dirs = build_common.profile.source_dirs
source_files = build_common.profile.source_files + ["./led.cpp", "./system_stm32f4xx.c", "syscalls.c"]
cflags = build_common.profile.cflags
cxxflags = build_common.profile.cxxflags
ldflags =  ["--static"] + build_common.profile.ldflags
linker_scripts = build_common.profile.linker_scripts
libraries = ["-lm", "-lc"]
arflags = list()

profile_main.name = "main"
profile_main.cc = "arm-none-eabi-gcc"
profile_main.cc_asm = "arm-none-eabi-as"
profile_main.cxx = "arm-none-eabi-g++"
profile_main.ld  = "arm-none-eabi-g++"
profile_main.objcopy = "arm-none-eabi-objcopy"
profile_main.objdump = "arm-none-eabi-objdump"
profile_main.include_dirs = include_dirs
profile_main.source_dirs = source_dirs
profile_main.source_files = source_files + ["./main.cpp"]
profile_main.cflags = cflags
profile_main.cxxflags = cxxflags
profile_main.ldflags = ldflags
profile_main.linker_scripts = linker_scripts
profile_main.libraries = libraries
profile_main.arflags = arflags
profile_main.executable = "main.elf"
profile_main.library = None
profile_main.verbose_level = make.verbosity.MINIMAL
profile_main.build_dir = "./build"
profile_main.executable_subtargets["iHex"] = [profile_main.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.hex"]
profile_main.executable_subtargets["binary"] = [profile_main.objcopy, "-O", "binary", 
    make.get_build_dir(profile_main)+profile_main.executable, make.get_build_dir(profile_main)+ "main.bin"]
profile_main.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_main) + "main.bin",
    "0x8000000"]

profile_bare_metal = copy.deepcopy(profile_main)
profile_bare_metal.name = "main_bare_metal"
profile_bare_metal.include_dirs = include_dirs
profile_bare_metal.source_dirs = source_dirs
profile_bare_metal.source_files = source_files + ["./main_bare_metal.cpp"]
profile_bare_metal.executable = "main_bare_metal.elf"
profile_bare_metal.executable_subtargets["iHex"] = [profile_bare_metal.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_bare_metal)+profile_bare_metal.executable, make.get_build_dir(profile_bare_metal)+ "main_bare_metal.hex"]
profile_bare_metal.executable_subtargets["binary"] = [profile_bare_metal.objcopy, "-O", "binary", 
    make.get_build_dir(profile_bare_metal)+profile_bare_metal.executable, make.get_build_dir(profile_bare_metal)+ "main_bare_metal.bin"]
profile_bare_metal.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_bare_metal) + "main_bare_metal.bin",
    "0x8000000"]

profile_freertos = copy.deepcopy(profile_main)
profile_freertos.name = "main_freertos"
profile_freertos.include_dirs = include_dirs
profile_freertos.source_dirs = source_dirs
profile_freertos.source_files = source_files + ["./main_freertos.cpp"]
profile_freertos.executable = "main_freertos.elf"
profile_freertos.executable_subtargets["iHex"] = [profile_freertos.objcopy, "-O", "ihex", 
    make.get_build_dir(profile_freertos)+profile_freertos.executable, make.get_build_dir(profile_freertos)+ "main_freertos.hex"]
profile_freertos.executable_subtargets["binary"] = [profile_freertos.objcopy, "-O", "binary", 
    make.get_build_dir(profile_freertos)+profile_freertos.executable, make.get_build_dir(profile_freertos)+ "main_freertos.bin"]
profile_freertos.additional_commands["prog"] = ["st-flash", "write", make.get_build_dir(profile_freertos) + "main_freertos.bin",
    "0x8000000"]

profiles = [profile_main, profile_bare_metal, profile_freertos]

make.build(profiles_list = profiles, args = sys.argv[1:])
