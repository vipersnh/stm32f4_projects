extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "main.h"
}

#include "led.hpp"


void led_initialize_all(void) 
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = LED_GREEN_PIN | LED_ORANGE_PIN | LED_RED_PIN | LED_BLUE_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(LED_GPIO, &GPIO_InitStructure);
}

void led_green_on(void) 
{
    GPIO_SetBits(LED_GPIO, LED_GREEN_PIN);
}

void led_green_off(void)
{
    GPIO_ResetBits(LED_GPIO, LED_GREEN_PIN);
}

void led_orange_on(void)
{
    GPIO_SetBits(LED_GPIO, LED_ORANGE_PIN);
}

void led_orange_off(void)
{
    GPIO_ResetBits(LED_GPIO, LED_ORANGE_PIN);
}

void led_red_on(void)
{
    GPIO_SetBits(LED_GPIO, LED_RED_PIN);
}

void led_red_off(void)
{
    GPIO_ResetBits(LED_GPIO, LED_RED_PIN);
}

void led_blue_on(void)
{
    GPIO_SetBits(LED_GPIO, LED_BLUE_PIN);
}

void led_blue_off(void)
{
    GPIO_ResetBits(LED_GPIO, LED_BLUE_PIN);
}

