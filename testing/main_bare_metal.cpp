#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "main.h"
}

#include "led.hpp"

void delay_ms(unsigned long long us) {
    #define ONE_MS_DELAY_COUNT  30000
    unsigned int delay = 0;
    while (us) {
        for (delay=0; delay<ONE_MS_DELAY_COUNT; delay++) {
        }
        us--;
    }
    return;
}


int main() {
    SystemInit();
    led_initialize_all();
    while (1) {
        led_red_on();
        delay_ms(50);
        led_red_off();
        delay_ms(50);
    }
    return -1;
}
