#define LED_GPIO GPIOD

#define LED_GREEN_PIN   GPIO_Pin_12
#define LED_ORANGE_PIN  GPIO_Pin_13
#define LED_RED_PIN     GPIO_Pin_14
#define LED_BLUE_PIN    GPIO_Pin_15


void led_initialize_all(void);

void led_green_on(void);
void led_green_off(void);

void led_orange_on(void);
void led_orange_off(void);

void led_red_on(void);
void led_red_off(void);

void led_blue_on(void);
void led_blue_off(void);
