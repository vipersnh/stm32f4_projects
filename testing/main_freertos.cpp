#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

extern "C" {
    #include "stm32f4xx_conf.h"
    #include "stm32f4xx.h"
    #include "main.h"
    #include "FreeRTOS.h"
    #include "task.h"
}

#include "led.hpp"

void delay_ms(unsigned long long us) {
    #define ONE_MS_DELAY_COUNT  30000
    unsigned int delay = 0;
    while (us) {
        for (delay=0; delay<ONE_MS_DELAY_COUNT; delay++) {
        }
        us--;
    }
    return;
}

void task_1(void *p) {
    while (1) {
        led_green_on();
        vTaskDelay(1000);
        led_green_off();
        vTaskDelay(1000);
    }
}

void task_2(void *p) {
    while (1) {
        led_orange_on();
        vTaskDelay(1000);
        led_orange_off();
        vTaskDelay(1000);
    }
}

void task_3(void *p) {
    while (1) {
        led_red_on();
        vTaskDelay(1000);
        led_red_off();
        vTaskDelay(1000);
    }
}

void task_4(void *p) {
    while (1) {
        led_blue_on();
        vTaskDelay(1000);
        led_blue_off();
        vTaskDelay(1000);
    }
}

int main() {
    SystemInit();
    led_initialize_all();
    xTaskCreate(task_1, (char *)"task_1", 100, 0 , tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_2, (char *)"task_2", 100, 0 , tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_3, (char *)"task_3", 100, 0 , tskIDLE_PRIORITY+1, 0);
    xTaskCreate(task_4, (char *)"task_4", 100, 0 , tskIDLE_PRIORITY+1, 0);
    vTaskStartScheduler();
    return -1;
}

